{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit ui.widget.common;

uses ui.curses;

type wid;
const wid_none : wid;
fn wid_is_valid~inline(id : wid) : bool;
implicit fn instance_eq_wid : class_eq(wid);

type appstate;
type property;

record event_set_property [
	prop : bytes;
	val : property;
]

option wevent [
	keyboard : event_keyboard;
	mouse : event_mouse;
	resize : event_resize;
	redraw : event_redraw;
	set_property : event_set_property;
	change_focus;
	property_changed : bytes;
	close;
	quit;
	noop;
]

record wid_wevent [
	id : wid;
	wev : wevent;
]

record widget_common [
	self : wid;
	parent : wid;
	x y : int;
	size_x size_y : int;
	off_x off_y : int;
	flt : bool;
	sub_widgets : list(wid);
]

record property_attrib [
	fg_r fg_g fg_b bg_r bg_g bg_b : uint16;
	attr mono : uint8;
]

option property [
	n;
	o : bool;
	i : int;
	r : rational;
	b : bytes;
	s : string;
	l : list(property);
	a : property_attrib;
	w : wid;
]

record widget_class [
	t : type;
	name : bytes;
	is_selectable : bool;
	get_width : fn(appstate, widget_common, t, int) : int;
	get_height : fn(appstate, widget_common, t, int) : int;
	get_property : fn(appstate, widget_common, t, bytes) : property;
	reflow : fn(appstate, widget_common, t) : (appstate, widget_common, t);
	redraw : fn(appstate, curses, widget_common, t) : curses;
	get_cursor : fn(appstate, widget_common, t) : (int, int);
	get_pivot : fn(appstate, widget_common, t) : (int, int);
	accepts_key : fn(appstate, widget_common, t, event_keyboard) : bool;
	process_event : fn(world, appstate, widget_common, t, wevent) : (world, appstate, widget_common, t);
]

option widget_align [
	left;
	center;
	right;
]

implicit fn instance_eq_property : class_eq(property);

fn widget_get_app(app : appstate) : wid;
fn widget_get_common(app : appstate, id : wid) : widget_common;
fn widget_get_class(app : appstate, id : wid) : bytes;
fn widget_get_underlying(app : appstate, id : wid) : wid;
fn widget_get_pivot(app : appstate, id : wid) : (int, int);
fn widget_get_width(app : appstate, id : wid, pref_x : int) : int;
fn widget_get_height(app : appstate, id : wid, x : int) : int;
fn widget_get_property(app : appstate, id : wid, prop : bytes) : property;
fn widget_send_async_event(w : world, app : appstate, id : wid, wev : wevent) : world;
fn widget_get_async_event_function(w : world, app : appstate, id : wid) : fn(ww : world, wev : wevent) : world;
fn widget_get_async_event_exchange_function(w : world, app : appstate, id : wid) : fn(ww : world, wev : wevent) : world;
fn widget_relative_mouse_coords(app : appstate, id : wid, m : event_mouse) : (int, int);
fn widget_enqueue_events(app : appstate, wev : list(wid_wevent)) : appstate;
fn widget_enqueue_event(app : appstate, id : wid, wev : wevent) : appstate;
fn widget_enqueue_event_to_underlying(app : appstate, id : wid, wev : wevent) : appstate;
fn widget_move(app : appstate, id : wid, x y size_x size_y off_x off_y : int) : appstate;
fn widget_place(app : appstate, id : wid, offs_x : int, size_x : int, yp : int) : (appstate, int);
fn widgets_get_width(app : appstate, ids : list(wid), x_space : int, x : int) : int;
fn widgets_place(app : appstate, ids : list(wid), align : widget_align, x_space y_space : int, offs_x : int, size_x : int, yp : int) : (appstate, int);
fn widget_is_top(app : appstate, id : wid) : bool;
fn widget_redraw_subwidgets(app : appstate, curs : curses, com : widget_common) : curses;
fn widget_get_cursor(implicit app : appstate, id : wid) : (int, int);
fn widget_accepts_key(app : appstate, id : wid,  wev : wevent) : bool;
fn widget_should_forward(app : appstate, com : widget_common, wev : wevent) : bool;
fn widget_activate(w : world, app : appstate, com : widget_common, id : wid) : (world, appstate, widget_common);
fn widget_forward(w : world, app : appstate, com : widget_common, wev : wevent) : (world, appstate, widget_common);
fn widget_new(w : world, app : appstate, parent : wid, const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t), flt : bool) : (world, appstate, wid);
fn widget_new_window(w : world, app : appstate, const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t), flt : bool) : (world, appstate, wid);
fn widget_destroy_onclick(app : appstate, id : wid) : appstate;

fn property_get(app : appstate, name : bytes) : property;
fn property_set(app : appstate, name : bytes, value : property) : appstate;
fn property_observe(app : appstate, id : wid, name : bytes) : appstate;
fn property_serialize(p : property) : bytes;
fn property_deserialize(b : bytes) : property;
fn property_save(app : appstate, names : list(bytes)) : bytes;
fn property_load(implicit app : appstate, names : list(bytes), str : bytes) : appstate;

fn properties_backup(app : appstate, names : list(bytes)) : appstate;
fn properties_revert(app : appstate, names : list(bytes)) : appstate;

fn new_property_attrib(fg_r fg_g fg_b bg_r bg_g bg_b : uint16, attr mono : uint8) : property_attrib;
fn property_get_attrib(app : appstate, name : bytes, fg_r fg_g fg_b bg_r bg_g bg_b : uint16, mono attr : uint8) : property_attrib;
fn property_set_attrib(a : property_attrib, curs : curses) : curses;

fn app_suspend(w : world, app : appstate) : (world, appstate);
fn app_resume(w : world, app : appstate) : (world, appstate);
fn app_run(w : world, d : dhandle, h : list(handle), env : treemap(bytes, bytes), const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t)) : world;

implementation

uses exception;

type wid := int32;
const wid_none : wid := -1;
fn wid_is_valid~inline(id : wid) : bool := id <> wid_none;

implicit fn instance_eq_wid : class_eq(wid) :=
	class_eq(wid).[
		equal : lambda(a b : wid) [ return a = b; ],
	];

record widget [
	common : widget_common;
	const cls : widget_class;
	state : cls.t;
]

record widget_property [
	val : property;
	observers : list(wid);
]

record appstate [
	d : dhandle;
	h : list(handle);
	env : treemap(bytes, bytes);
	widgets : list(widget);
	wm_wid : wid;
	app_wid : wid;
	curs : curses;
	events : list(wid_wevent);
	properties : list(widget_property);
	async_mq : msgqueue(wevent);
]

fn widget_get_app(app : appstate) : wid
[
	return app.app_wid;
]

fn widget_get_common(app : appstate, id : wid) : widget_common
[
	return app.widgets[id].common;
]

fn widget_get_class(app : appstate, id : wid) : bytes
[
	return app.widgets[id].cls.name;
]

fn widget_get_underlying(app : appstate, id : wid) : wid
[
	const wi := app.widgets[id];
	const pa := app.widgets[wi.common.parent];
	var u := wid_none;
	for i := 0 to len(pa.common.sub_widgets) do [
		if pa.common.sub_widgets[i] = id then
			return u;
		u := pa.common.sub_widgets[i];
	]
	abort internal("widget_get_underlying: widget not found in the parent list");
]

fn widget_get_pivot(app : appstate, id : wid) : (int, int)
[
	const wi := app.widgets[id];
	const pa := app.widgets[wi.common.parent];
	var pivot_x, pivot_y := 0, 0;
	for i := 0 to len(pa.common.sub_widgets) do [
		if pa.common.sub_widgets[i] = id then [
			return pivot_x, pivot_y;
		]
		const un := app.widgets[pa.common.sub_widgets[i]];
		if not is_uninitialized_record(un.cls.get_pivot) then [
			pivot_x, pivot_y := un.cls.get_pivot(app, un.common, un.state);
			pivot_x += un.common.x;
			pivot_y += un.common.y;
		]
	]
	abort internal("widget_get_pivot: widget not found in the parent list");
]

fn widget_get_width(app : appstate, id : wid, pref_x : int) : int
[
	const wi := app.widgets[id];
	if is_uninitialized_record(wi.cls.get_width) then
		return pref_x;
	return wi.cls.get_width(app, wi.common, wi.state, pref_x);
]

fn widget_get_height(app : appstate, id : wid, x : int) : int
[
	const wi := app.widgets[id];
	if is_uninitialized_record(wi.cls.get_height) then
		return 1;
	return wi.cls.get_height(app, wi.common, wi.state, x);
]

fn widget_send_async_event(implicit w : world, app : appstate, id : wid, wev : wevent) : world
[
	msgqueue_send(app.async_mq, id, wev);
]

fn widget_get_async_event_function(w : world, app : appstate, id : wid) : fn(ww : world, wev : wevent) : world
[
	return lambda(ww : world, wev : wevent) : world [
		return msgqueue_send(ww, wevent, app.async_mq, id, wev);
	];
]

fn widget_get_async_event_exchange_function(w : world, app : appstate, id : wid) : fn(ww : world, wev : wevent) : world
[
	return lambda(ww : world, wev : wevent) : world [
		var nw, xtag, xev := msgqueue_receive_tag_nonblock(ww, wevent, app.async_mq, id);
		eval nw;
		return msgqueue_send(ww, wevent, app.async_mq, id, wev);
	];
]

fn widget_get_property(app : appstate, id : wid, prop : bytes) : property
[
	const wi := app.widgets[id];
	if is_uninitialized_record(wi.cls.get_property) then
		return property.n;
	return wi.cls.get_property(app, wi.common, wi.state, prop);
]

fn widget_relative_mouse_coords(app : appstate, id : wid, m : event_mouse) : (int, int)
[
	var x := m.x;
	var y := m.y;
	while id <> wid_none do [
		var com := app.widgets[id].common;
		x -= com.x;
		y -= com.y;
		x -= com.off_x;
		y -= com.off_y;
		id := com.parent;
	]
	return x, y;
]

fn widget_enqueue_events(implicit app : appstate, wev : list(wid_wevent)) : appstate
[
	app.events := wev + app.events;
]

fn widget_enqueue_event(implicit app : appstate, id : wid, wev : wevent) : appstate
[
	return widget_enqueue_events([ wid_wevent.[
		id : id,
		wev : wev,
	] ]);
]

fn widget_enqueue_event_to_underlying(implicit app : appstate, id : wid, wev : wevent) : appstate
[
	var u := widget_get_underlying(id);
	if wid_is_valid(u) then
		widget_enqueue_event(u, wev);
]

fn widget_move(implicit app : appstate, id : wid, x y size_x size_y off_x off_y : int) : appstate
[
	app.widgets[id].common.x := x;
	app.widgets[id].common.y := y;
	app.widgets[id].common.size_x := size_x;
	app.widgets[id].common.size_y := size_y;
	app.widgets[id].common.off_x := off_x;
	app.widgets[id].common.off_y := off_y;
	if not is_uninitialized_record(app.widgets[id].cls.reflow) then [
		mutable wi := app.widgets[id];
		wi.common, wi.state := wi.cls.reflow(wi.common, wi.state);
		app.widgets[id] := wi;
	]
]

fn widget_place(implicit app : appstate, id : wid, offs_x : int, size_x : int, yp : int) : (appstate, int)
[
	var ys := widget_get_height(id, size_x);
	widget_move(id, offs_x, yp, size_x, ys, 0, 0);
	return yp + ys;
]

record widget_flow_line [
	ids : list(wid);
	x : list(int);
	width : list(int);
	x_size : int;
]

fn widgets_flow(implicit app : appstate, ids : list(wid), x_space : int, pref_x : int) : list(widget_flow_line)
[
	var flow := empty(widget_flow_line);
	var flow_line := widget_flow_line.[ ids : empty(wid), x : empty(int), width : empty(int), x_size : 0 ];
	var x := 0;
	for i := 0 to len(ids) do [
		var l := widget_get_width(ids[i], pref_x);
		if x > 0, x + x_space + l > pref_x then [
			flow +<= flow_line;
			flow_line := widget_flow_line.[ ids : empty(wid), x : empty(int), width : empty(int), x_size : 0 ];
			x := 0;
		]
		x += select(x > 0, 0, x_space);
		flow_line.ids +<= ids[i];
		flow_line.x +<= x;
		flow_line.width +<= l;
		x += l;
		flow_line.x_size := x;
	]
	if x > 0 then
		flow +<= flow_line;
	return flow;
]

fn widgets_get_width(implicit app : appstate, ids : list(wid), x_space : int, pref_x : int) : int
[
	var flow := widgets_flow(ids, x_space, pref_x);

	var max_x := 0;
	for i := 0 to len(flow) do [
		max_x := max(max_x, flow[i].x_size);
	]

	return max_x;
]

fn widgets_place(implicit app : appstate, ids : list(wid), align : widget_align, x_space y_space : int, offs_x : int, size_x : int, yp : int) : (appstate, int)
[
	var flow := widgets_flow(ids, x_space, size_x);
	for i := 0 to len(flow) do [
		if i > 0 then
			yp += y_space;
		var flow_line := flow[i];
		var x_offset := offs_x;
		if align is center then
			x_offset += size_x - flow_line.x_size shr 1;
		if align is right then
			x_offset += size_x - flow_line.x_size;
		var y_size := 0;
		for j := 0 to len(flow_line.ids) do [
			var yh := widget_get_height(flow_line.ids[j], flow_line.width[j]);
			widget_move(flow_line.ids[j], x_offset + flow_line.x[j], yp, flow_line.width[j], yh, 0, 0);
			y_size := max(y_size, yh);
		]
		yp += y_size;
	]
	return yp;
]

fn widget_get_top(implicit app : appstate, id : wid) : wid
[
	if not wid_is_valid(id) then
		return wid_none;
	const wi := app.widgets[id];
	var idx := len(wi.common.sub_widgets) - 1;
	while idx >= 0, app.widgets[wi.common.sub_widgets[idx]].common.flt do
		idx -= 1;
	if idx >= 0 then
		return wi.common.sub_widgets[idx];
	return wid_none;
]

fn widget_is_top(implicit app : appstate, id : wid) : bool
[
	var top := widget_get_top(app.widgets[id].common.parent);
	return id = top;
]

fn wi_to_rect(com : widget_common) : rect
[
	return rect.[ x1 : com.x, x2 : com.x + com.size_x, y1 : com.y, y2 : com.y + com.size_y ];
]

fn widget_redraw_subwidgets(implicit app : appstate, implicit curs : curses, com : widget_common) : curses
[
	for i := 0 to len(com.sub_widgets) do [
		var id := com.sub_widgets[i];
		const wi := app.widgets[id];
		var sc := curses_get_scissors();
		sc := rect_intersection(sc, wi_to_rect(wi.common));
		var rs := [ sc ];
		for j := i + 1 to len(com.sub_widgets) do [
			var id2 := com.sub_widgets[j];
			const wi2 := app.widgets[id2];
			var r := wi_to_rect(wi2.common);
			rs := rect_set_exclude(rs, r);
		]
		var r := rect_set_to_rect(rs);
		curses_restrict_viewport(r.x1, r.x2, r.y1, r.y2, wi.common.x, wi.common.y);
		if curses_valid_viewport() then
			wi.cls.redraw(wi.common, wi.state);
		curses_revert_viewport();
	]
]

fn widget_get_cursor(implicit app : appstate, id : wid) : (int, int)
[
	var cx, cy := -1, -1;
	var xo, yo := 0, 0;
go_down:
	const wi := app.widgets[id];
	xo += wi.common.x + wi.common.off_x;
	yo += wi.common.y + wi.common.off_y;
	if not is_uninitialized_record(wi.cls.get_cursor) then [
		var x, y := wi.cls.get_cursor(wi.common, wi.state);
		if x >= 0, y >= 0 then
			cx, cy := x + xo, y + yo;
		return cx, cy;
	]
	id := widget_get_top(id);
	if wid_is_valid(id) then
		goto go_down;
	return cx, cy;
]

fn set_cursor(implicit app : appstate, implicit curs : curses) : curses
[
	curses_set_cursor(widget_get_cursor(app.wm_wid));
]

fn widget_accepts_key(implicit app : appstate, id : wid, wev : wevent) : bool
[
	if wev is keyboard then [
		const wi := app.widgets[id];
		if is_uninitialized_record(wi.cls.accepts_key) then
			return true;
		return wi.cls.accepts_key(wi.common, wi.state, wev.keyboard);
	]
	return false;
]

fn widget_should_forward(implicit app : appstate, implicit com : widget_common, wev : wevent) : bool
[
	var l := len(com.sub_widgets);
	if l = 0 then
		return false;
	return widget_accepts_key(com.sub_widgets[l - 1], wev);
]

fn widget_select(implicit w : world, implicit app : appstate, implicit com : widget_common, wev : wevent) : (world, appstate, widget_common)
[
	var l := len(com.sub_widgets);
	var old_top := com.sub_widgets[l - 1];
	var fwd : bool;
	if wev is keyboard then [
		if wev.keyboard.key = key_right or wev.keyboard.key = key_down or (wev.keyboard.key = key_tab and wev.keyboard.flags = 0) then [
			fwd := true;
			goto rotate;
		]
		if wev.keyboard.key = key_left or wev.keyboard.key = key_up or (wev.keyboard.key = key_tab and wev.keyboard.flags <> 0) then [
			fwd := false;
			goto rotate;
		]
	]
	if wev is mouse, wev.mouse.buttons <> 0 or wev.mouse.prev_buttons <> 0 or wev.mouse.wx <> 0 or wev.mouse.wy <> 0 then [
		var sw : wid;
		var mx, my := widget_relative_mouse_coords(com.self, wev.mouse);
		for i := 0 to l do [
			sw := com.sub_widgets[i];
			var r := rect.[
				x1 : app.widgets[sw].common.x,
				x2 : app.widgets[sw].common.x + app.widgets[sw].common.size_x,
				y1 : app.widgets[sw].common.y,
				y2 : app.widgets[sw].common.y + app.widgets[sw].common.size_y,
			];
			if app.widgets[sw].cls.is_selectable, rect_test_point(r, mx, my) then
				goto have_it;
		]
		return;
have_it:
		while com.sub_widgets[l - 1] <> sw do [
			com.sub_widgets := [ com.sub_widgets[l - 1] ] + com.sub_widgets[ .. l - 1];
		]
		widget_enqueue_event(sw, wev);
		goto new_top_redraw;
	]
	return;

rotate:
	if fwd then [
		com.sub_widgets := [ com.sub_widgets[l - 1] ] + com.sub_widgets[ .. l - 1];
	] else [
		com.sub_widgets := com.sub_widgets[1 .. ] + [ com.sub_widgets[0] ];
	]
	if not app.widgets[com.sub_widgets[l - 1]].cls.is_selectable then
		goto rotate;

new_top_redraw:
	var new_top := com.sub_widgets[l - 1];
	if old_top = new_top then
		return;

	widget_enqueue_event(new_top, wevent.change_focus);
	widget_enqueue_event(old_top, wevent.change_focus);
]

fn widget_activate(implicit w : world, implicit app : appstate, implicit com : widget_common, id : wid) : (world, appstate, widget_common)
[
	var l := len(com.sub_widgets);
	var old_top := com.sub_widgets[l - 1];

	while com.sub_widgets[l - 1] <> id do [
		com.sub_widgets := [ com.sub_widgets[l - 1] ] + com.sub_widgets[ .. l - 1];
	]

	var new_top := com.sub_widgets[l - 1];
	if old_top = new_top then
		return;

	widget_enqueue_event(new_top, wevent.change_focus);
	widget_enqueue_event(old_top, wevent.change_focus);
]

fn widget_forward(implicit w : world, implicit app : appstate, implicit com : widget_common, wev : wevent) : (world, appstate, widget_common)
[
	if widget_should_forward(wev) then [
		widget_enqueue_event(com.sub_widgets[len(com.sub_widgets) - 1], wev);
		return;
	]
	if wev is keyboard or wev is mouse then [
		widget_select(wev);
	]
]

fn widget_new(implicit w : world, implicit app : appstate, parent : wid, const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t), flt : bool) : (world, appstate, wid)
[
	var id : wid := len(app.widgets);
	app.widgets +<= widget.[
		common : widget_common.[
			self : id,
			parent : parent,
			x : 0,
			y : 0,
			size_x : 0,
			size_y : 0,
			off_x : 0,
			off_y : 0,
			flt : flt,
			sub_widgets : empty(wid),
		],
		cls : cls,
	];
	var x : cls.t;
	x := init(id);
	mutable wi := widget.[
		common : app.widgets[id].common,
		cls : cls,
		state : x,
	];
	app.widgets[id] := wi;
	if wid_is_valid(parent) then [
		var pos := len(app.widgets[parent].common.sub_widgets);
		while pos > 0, app.widgets[app.widgets[parent].common.sub_widgets[pos - 1]].common.flt do
			pos -= 1;
		app.widgets[parent].common.sub_widgets := app.widgets[parent].common.sub_widgets[ .. pos] + [ id ] + app.widgets[parent].common.sub_widgets[pos .. ];
	]
	return id;
]

fn widget_new_window(implicit w : world, implicit app : appstate, const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t), flt : bool) : (world, appstate, wid)
[
	var old_top := widget_get_top(app.wm_wid);
	var id := widget_new(app.wm_wid, cls, init, flt);
	widget_enqueue_event(id, wevent.resize.(event_resize.[
		x : app.widgets[app.wm_wid].common.size_x,
		y : app.widgets[app.wm_wid].common.size_y,
	]));
	var new_top := widget_get_top(app.wm_wid);
	if old_top <> new_top then [
		if wid_is_valid(old_top) then
			widget_enqueue_event(old_top, wevent.change_focus);
		if wid_is_valid(new_top) then
			widget_enqueue_event(new_top, wevent.change_focus);
	]
	return id;
]

fn widget_destroy_onclick(implicit app : appstate, id : wid) : appstate
[
	if app.widgets[app.widgets[id].common.parent].cls.name = "dialog" then [
		widget_enqueue_event(app.widgets[id].common.parent, wevent.close);
	] else [
		var events := empty(wid_wevent);
		while app.widgets[id].cls.name = "menu" or app.widgets[id].cls.name = "mainmenu" do [
			events +<= wid_wevent.[
				id : id,
				wev : wevent.close,
			];
			id := widget_get_underlying(id);
		]
		widget_enqueue_events(events);
	]
]

record wm_state [
]

fn wm_init(implicit w : world, implicit app : appstate, id : wid) : (world, appstate, wm_state)
[
	return wm_state.[ ];
]


fn wm_redraw(implicit app : appstate, implicit curs : curses, com : widget_common, st : wm_state) : curses
[
	widget_redraw_subwidgets(com);
]

fn wm_process_event(implicit w : world, implicit app : appstate, implicit com : widget_common, implicit st : wm_state, wev : wevent) : (world, appstate, widget_common, wm_state)
[
	//eval debug("wm_process_event: received event");
	if wev is resize then [
		com.size_x := wev.resize.x;
		com.size_y := wev.resize.y;
		var wwl := empty(wid_wevent);
		for i := 0 to len(com.sub_widgets) do [
			wwl +<= wid_wevent.[
				id : com.sub_widgets[i],
				wev : wev,
			];
		]
		wwl +<= wid_wevent.[
			id : com.self,
			wev : wevent.redraw.(event_redraw.[
				x1 : 0,
				y1 : 0,
				x2 : wev.resize.x,
				y2 : wev.resize.y,
			]),
		];
		widget_enqueue_events(wwl);
		return;
	]
	if len(com.sub_widgets) = 0 then
		return;
	const sub := app.widgets[com.sub_widgets[len(com.sub_widgets) - 1]];
	widget_enqueue_event(sub.common.self, wev);
]

const wm_class~flat := widget_class.[
	t : wm_state,
	name : "wm",
	is_selectable : true,
	redraw : wm_redraw,
	process_event : wm_process_event,
];

fn get_redraw_rect(implicit app : appstate, id : wid, r : rect) : rect
[
	while id <> wid_none do [
		var com := app.widgets[id].common;
		r.x1 += com.x;
		r.x2 += com.x;
		r.y1 += com.y;
		r.y2 += com.y;
		id := com.parent;
		if id <> wid_none then [
			var com := app.widgets[id].common;
			r.x1 += com.off_x;
			r.x2 += com.off_x;
			r.y1 += com.off_y;
			r.y2 += com.off_y;
		]
	];
	return r;
]

fn do_redraw(implicit app : appstate, r : rect) : appstate
[
	const wm := app.widgets[app.wm_wid];
	implicit curs := app.curs;
	curses_restrict_viewport(r.x1, r.x2, r.y1, r.y2, 0, 0);
	wm.cls.redraw(wm.common, wm.state);
	curses_revert_viewport();
	app.curs := curs;
]

fn delete_widget(implicit app : appstate, id : wid) : appstate
[
	const wi := app.widgets[id];
	for i := 0 to len(wi.common.sub_widgets) do [
		delete_widget(wi.common.sub_widgets[i]);
	]
	var parent := wi.common.parent;
	var old_top := widget_get_top(parent);
	const pa := app.widgets[parent];
	for i := 0 to len(pa.common.sub_widgets) do [
		if pa.common.sub_widgets[i] = id then [
			app.widgets[parent].common.sub_widgets := app.widgets[parent].common.sub_widgets[ .. i] + app.widgets[parent].common.sub_widgets[i + 1 .. ];
			app.widgets[id] := uninitialized(widget);
			var new_top := widget_get_top(parent);
			if old_top <> new_top then [
				if wid_is_valid(old_top) then
					widget_enqueue_event(old_top, wevent.change_focus);
				if wid_is_valid(new_top) then
					widget_enqueue_event(new_top, wevent.change_focus);
			]
			return;
		]
	]
	abort internal("delete_widget: widget not found in the parent list");
]

fn property_equal(a b : property) : bool
[
	if ord a <> ord b then
		return false;
	if a is n then
		return true;
	if a is o then
		return a.o = b.o;
	if a is i then
		return a.i = b.i;
	if a is r then
		return a.r = b.r;
	if a is b then
		return a.b = b.b;
	if a is s then
		return a.s = b.s;
	if a is l then
		return a.l = b.l;
	if a is a then
		return	a.a.fg_r = b.a.fg_r and
			a.a.fg_g = b.a.fg_g and
			a.a.fg_b = b.a.fg_b and
			a.a.bg_r = b.a.bg_r and
			a.a.bg_g = b.a.bg_g and
			a.a.bg_b = b.a.bg_b and
			a.a.attr = b.a.attr and
			a.a.mono = b.a.mono;
	if a is w then
		return a.w = b.w;
	abort internal("property_equal: impossible option type " + ntos(ord a));
]

implicit fn instance_eq_property : class_eq(property) :=
	class_eq(property).[
		equal : property_equal,
	];

fn property_to_id(name : bytes) : int
[
	var result := 0;
	for i := 0 to len(name) do [
		var c := name[i];
		result := (result shl 8) + c;
	]
	return result;
]

fn property_get(app : appstate, name : bytes) : property
[
	var n := property_to_id(name);
	var p := app.properties[n].val;
	if is_uninitialized(p) then
		return property.n;
	{
	var s := property_serialize(p);
	var p2 := property_deserialize(s);
	if p <> p2 then
		abort internal("error deserializing " + ntos(ord p) + " -> " + ntos(ord p2));
	}
	return p;
]

fn property_set(implicit app : appstate, name : bytes, value : property) : appstate
[
	var n := property_to_id(name);
	if is_uninitialized(app.properties[n]) then [
		app.properties[n] := widget_property.[
			val : property.n,
			observers : empty(wid),
		];
	]

	if app.properties[n].val <> value then [
		app.properties[n].val := value;

		var i := 0;
		while i < len(app.properties[n].observers) do [
			var id := app.properties[n].observers[i];
			if is_uninitialized(app.widgets[id]) then [
				app.properties[n].observers := app.properties[n].observers[ .. i] + app.properties[n].observers[i + 1 .. ];
				continue;
			]
			widget_enqueue_event(id, wevent.property_changed.(name));
			i += 1;
		]
	]
]

fn property_observe(app : appstate, id : wid, name : bytes) : appstate
[
	var n := property_to_id(name);
	if is_uninitialized(app.properties[n]) then [
		app.properties[n] := widget_property.[
			val : property.n,
			observers : empty(wid),
		];
	]
	app.properties[n].observers +<= id;
	return app;
]

fn property_serialize(p : property) : bytes
[
	if p is n then
		return "n";
	if p is o then
		return "o:" + select(p.o, "0", "1");
	if p is i then
		return "i:" + ntos(p.i);
	if p is r then
		return "r:" + ntos(p.r.num) + "." + ntos(p.r.den);
	if p is b then [
		var r := "b:";
		for x in p.b do
			r += ntos(x) + ".";
		return r;
	]
	if p is s then [
		var r := "s:";
		for x in p.s do
			r += ntos(x) + ".";
		return r;
	]
	if p is l then [
		var r:= "l:";
		for x in p.l do
			r += "(" + property_serialize(x) + ")";
		return r;
	]
	if p is a then [
		return "a:" + ntos(p.a.fg_r) + "." + ntos(p.a.fg_g) + "." + ntos(p.a.fg_b) + "." +
			      ntos(p.a.bg_r) + "." + ntos(p.a.bg_g) + "." + ntos(p.a.bg_b) + "." +
			      ntos(p.a.attr) + "." + ntos(p.a.mono);
	]
	if p is w then
		return "w:" + ntos(p.w);
	abort internal("property_serialize: impossible option type " + ntos(ord p));
]

fn property_deserialize(b : bytes) : property
[
	if b = "n" then
		return property.n;
	if b = "o:0" then
		return property.o.(false);
	if b = "o:1" then
		return property.o.(true);
	if b[0] = 'i', b[1] = ':' then
		return property.i.(ston(b[2 .. ]));
	if b[0] = 'r', b[1] = ':' then [
		b := b[2 .. ];
		var idx := list_search(b, '.');
		return property.r.(rational.[ num : ston(b[ .. idx]), den : ston(b[idx + 1 .. ])]);
	]
	if b[0] = 'b', b[1] = ':' then [
		var l := list_break(b[2 .. ], '.');
		var res := empty(byte);
		for x in l do
			res +<= ston(x);
		return property.b.(res);
	]
	if b[0] = 's', b[1] = ':' then [
		var l := list_break(b[2 .. ], '.');
		var res := empty(char);
		for x in l do
			res +<= ston(x);
		return property.s.(res);
	]
	if b[0] = 'l', b[1] = ':' then [
		var res := empty(property);
		b := b[2 .. ];
		while len_greater_than(b, 0) do [
			var depth := 0;
			var idx : int;
			for i := 0 to len(b) do [
				if b[i] = '(' then
					depth += 1;
				if b[i] = ')' then [
					depth -= 1;
					if depth = 0 then [
						idx := i;
						goto have_it;
					]
				]
			]
			abort;
have_it:
			res +<= property_deserialize(b[1 .. idx]);
			b := b[idx + 1 .. ];
		]
		return property.l.(res);
	]
	if b[0] = 'a', b[1] = ':' then [
		var l := list_break(b[2 .. ], '.');
		var n := map(l, ston);
		return property.a.(property_attrib.[
			fg_r : n[0],
			fg_g : n[1],
			fg_b : n[2],
			bg_r : n[3],
			bg_g : n[4],
			bg_b : n[5],
			attr : n[6],
			mono : n[7],
		]);
	]
	if b[0] = 'w', b[1] = ':' then [
		return property.w.(ston(b[2 .. ]));
	]
	abort;
]

fn property_save(implicit app : appstate, names : list(bytes)) : bytes
[
	var saved := "";
	for n in names do [
		var p := property_get(n);
		var s := property_serialize(p);
		saved += n + ":" + s + nl;
	]
	return saved;
]

fn property_load(implicit app : appstate, names : list(bytes), str : bytes) : appstate
[
	var ts := treeset_from_list(names);
	var lines := list_break_to_lines(str);
	for line in lines do [
		var c := list_search(line, ':');
		if c = -1 then
			continue;
		var name := line[ .. c];
		if not treeset_test(ts, name) then
			continue;
		var prop := property_deserialize(line[c + 1 .. ]);
		if not is_exception prop then
			property_set(name, prop);
	]
]

fn properties_backup(implicit app : appstate, names : list(bytes)) : appstate
[
	for i := 0 to len(names) do [
		property_set("backup-" + names[i], property_get(names[i]));
	]
]

fn properties_revert(implicit app : appstate, names : list(bytes)) : appstate
[
	for i := 0 to len(names) do [
		property_set(names[i], property_get("backup-" + names[i]));
	]
]

fn new_property_attrib(fg_r fg_g fg_b bg_r bg_g bg_b : uint16, attr mono : uint8) : property_attrib
[
	return property_attrib.[
		fg_r : fg_r,
		fg_g : fg_g,
		fg_b : fg_b,
		bg_r : bg_r,
		bg_g : bg_g,
		bg_b : bg_b,
		attr : attr,
		mono : mono,
	];
]

fn property_get_attrib(implicit app : appstate, name : bytes, fg_r fg_g fg_b bg_r bg_g bg_b : uint16, attr mono : uint8) : property_attrib
[
	var p := property_get(name);
	if p is a then
		return p.a;
	else
		return new_property_attrib(fg_r, fg_g, fg_b, bg_r, bg_g, bg_b, attr, mono);
]

fn property_set_attrib(a : property_attrib, implicit curs : curses) : curses
[
	if curses_n_colors() >= 16 then [
		curses_set_fgcolor(a.fg_r, a.fg_g, a.fg_b);
		curses_set_bgcolor(a.bg_r, a.bg_g, a.bg_b);
		curses_set_attributes(a.attr);
	] else [
		curses_set_fgcolor(#aaaa, #aaaa, #aaaa);
		curses_set_bgcolor(#0000, #0000, #0000);
		curses_set_attributes(a.mono);
	]
]

fn app_suspend(implicit w : world, implicit app : appstate) : (world, appstate)
[
	curses_done(app.curs);
	app.curs := exception_make(curses, ec_sync, error_record_field_not_initialized, 0, false);
]

fn app_resume(implicit w : world, implicit app : appstate) : (world, appstate)
[
	app.curs := curses_init(app.d, app.h, app.env);
]

fn app_run(implicit w : world, d : dhandle, h : list(handle), env : treemap(bytes, bytes), const cls : widget_class, init : fn(world, appstate, wid) : (world, appstate, cls.t)) : world
[
	var async_mq := msgqueue_new(wevent);
	implicit app := appstate.[
		d : d,
		h : h,
		env : env,
		widgets : empty(widget),
		wm_wid : wid_none,
		events : empty(wid_wevent),
		properties : infinite_uninitialized(widget_property),
		async_mq : async_mq,
	];

	app_resume();

	app.wm_wid := widget_new(wid_none, wm_class, wm_init, false);

	app.app_wid := widget_new(app.wm_wid, cls, init, false);

	//property_set("test", property.b.("hello"));
	//var val := property_get("test");
	//eval debug(val.b);
	//sleep(w, 1000000);

	while true do [
		var quit := false;
		while len_greater_than(app.events, 0) do [
			var ww := app.events[0];
			app.events := app.events[1 .. ];
			if ww.id >= 0, is_uninitialized(app.widgets[ww.id]) then
				continue;
			if ww.wev is quit then [
				quit := true;
				continue;
			]
			if ww.wev is redraw then [
				var r := rect.[
					x1 : ww.wev.redraw.x1,
					x2 : ww.wev.redraw.x2,
					y1 : ww.wev.redraw.y1,
					y2 : ww.wev.redraw.y2,
				];
				r := get_redraw_rect(ww.id, r);
				do_redraw(r);
				continue;
			]
			if ww.wev is close then [
				var r := rect.[
					x1 : 0,
					x2 : app.widgets[ww.id].common.size_x,
					y1 : 0,
					y2 : app.widgets[ww.id].common.size_y,
				];
				r := get_redraw_rect(ww.id, r);
				delete_widget(ww.id);
				do_redraw(r);
				continue;
			]
			if not is_uninitialized_record(app.widgets[ww.id].cls.process_event) then [
				mutable wi := app.widgets[ww.id];
				wi.common, wi.state := wi.cls.process_event(wi.common, wi.state, ww.wev);
				app.widgets[ww.id] := wi;
			]
		]
		if quit then
			break;
		app.curs := set_cursor(app.curs);
		var ev : event;
		app.curs, ev := curses_get_event(app.curs, wevent, async_mq);
		var wev := wevent.noop;
		if ev is altmq then [
			var id, wev := msgqueue_receive(async_mq);
			app.events := [ wid_wevent.[ id : id, wev : wev ] ];
			continue;
		] if ev is keyboard then [
			wev.keyboard := ev.keyboard;
		] else if ev is mouse then [
			wev.mouse := ev.mouse;
			wev.mouse.x := max(min(wev.mouse.x, app.widgets[app.wm_wid].common.size_x - 1), 0);
			wev.mouse.y := max(min(wev.mouse.y, app.widgets[app.wm_wid].common.size_y - 1), 0);
		] else if ev is redraw then [
			wev.redraw := ev.redraw;
		] else if ev is resize then [
			wev.resize := ev.resize;
		] else if ev is suspend then [
			var ctrl_z, ctrl_z_token := signal_handle("SIGTSTP");
			app_suspend();
			tty_background();
try_fg:
			var b := tty_foreground();
			if not b then [
				sleep(w, 1000000);
				goto try_fg;
			]
			app_resume();
			signal_unhandle(ctrl_z);
		] else [
			continue;
		]
		app.events := [ wid_wevent.[ id : app.wm_wid, wev : wev ] ];
	]
	app.curs := curses_update(app.curs);

	app_suspend();

	return w;
]

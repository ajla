{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

unit ui.rect;

record rect [
	x1 x2 y1 y2 : int;
]

type rect_set := list(rect);

fn rect_is_valid~inline(r : rect) : bool;
fn rect_test_point~inline(r : rect, x y : int) : bool;
fn rect_unite(r1 r2 : rect) : rect;
fn rect_intersection~inline(r1 r2 : rect) : rect;
fn rect_test_intersect(r1 r2 : rect) : bool;
fn rect_set_test_point(rs : rect_set, x y : int) : bool;
fn rect_set_add(rs : rect_set, r : rect) : rect_set;
fn rect_set_exclude(rs : rect_set, r : rect) : rect_set;
fn rect_set_intersect(rs : rect_set, r : rect) : rect_set;
fn rect_set_move(rs : rect_set, offset_x offset_y : int) : rect_set;
fn rect_set_to_rect(rs : rect_set) : rect;

implicit fn instance_eq_rect : class_eq(rect);

implementation

fn rect_is_valid~inline(r : rect) : bool
[
	return r.x1 < r.x2 and r.y1 < r.y2;
]

fn rect_test_point~inline(r : rect, x y : int) : bool
[
	return x >= r.x1 and x < r.x2 and y >= r.y1 and y < r.y2;
]

fn rect_unite(r1 r2 : rect) : rect
[
	if not rect_is_valid(r1) then
		return r2;
	if not rect_is_valid(r2) then
		return r1;
	return rect.[
		x1 : min(r1.x1, r2.x1),
		x2 : max(r1.x2, r2.x2),
		y1 : min(r1.y1, r2.y1),
		y2 : max(r1.y2, r2.y2),
	];
]

fn rect_intersection~inline(r1 r2 : rect) : rect
[
	return rect.[
		x1 : max(r1.x1, r2.x1),
		x2 : min(r1.x2, r2.x2),
		y1 : max(r1.y1, r2.y1),
		y2 : min(r1.y2, r2.y2),
	];
]

fn rect_test_intersect(r1 r2 : rect) : bool
[
	return rect_is_valid(rect_intersection(r1, r2));
]

fn rect_set_test_point(rs : rect_set, x y : int) : bool
[
	for i := 0 to len(rs) do [
		var rr := rs[i];
		if rect_test_point(rr, x, y) then
			return true;
	]
	return false;
]

fn rect_set_add(rs : rect_set, r : rect) : rect_set
[
	if rect_is_valid(r) then
		rs +<= r;
	return rs;
]

fn rect_set_exclude(rs : rect_set, r : rect) : rect_set
[
	var result := empty(rect);
	for i := 0 to len(rs) do [
		var rr := rs[i];
		if not rect_test_intersect(r, rr) then [
			result := rect_set_add(result, rr);
			continue;
		]
		var r1 := rect.[ x1 : rr.x1, x2 : rr.x2, y1 : rr.y1, y2 : r.y1 ];
		var r2 := rect.[ x1 : rr.x1, x2 : r.x1, y1 : r.y1, y2 : r.y2 ];
		var r3 := rect.[ x1 : r.x2, x2 : rr.x2, y1 : r.y1, y2 : r.y2 ];
		var r4 := rect.[ x1 : rr.x1, x2 : rr.x2, y1 : r.y2, y2 : rr.y2 ];
		r2 := rect_intersection(r2, rr);
		r3 := rect_intersection(r3, rr);
		result := rect_set_add(result, r1);
		result := rect_set_add(result, r2);
		result := rect_set_add(result, r3);
		result := rect_set_add(result, r4);
	]
	return result;
]

fn rect_set_intersect(rs : rect_set, r : rect) : rect_set
[
	var result := empty(rect);
	for i := 0 to len(rs) do [
		var rr := rs[i];
		rr := rect_intersection(rr, r);
		result := rect_set_add(result, rr);
	]
	return result;
]

fn rect_set_move(rs : rect_set, offset_x offset_y : int) : rect_set
[
	for i := 0 to len(rs) do [
		rs[i].x1 += offset_x;
		rs[i].x2 += offset_x;
		rs[i].y1 += offset_y;
		rs[i].y2 += offset_y;
	]
	return rs;
]

fn rect_set_to_rect(rs : rect_set) : rect
[
	if len(rs) = 0 then
		return rect.[ x1 : 0, x2 : 0, y1 : 0, y2 : 0 ];
	var r := rs[0];
	for i := 1 to len(rs) do
		r := rect_unite(r, rs[i]);
	return r;
]

fn rect_equal(r1 r2 : rect) : bool :=
	r1.x1 = r2.x1 and r1.x2 = r2.x2 and r1.y1 = r2.y1 and r1.y2 = r2.y2;

implicit fn instance_eq_rect : class_eq(rect) :=
	class_eq(rect).[
		equal : rect_equal,
	];

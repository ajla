{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.parser.istring;

type istring := int128;

fn i_encode(b : bytes) : istring;
fn i_append(result : istring, b : bytes) : istring;
fn i_decode(i : istring) : bytes;

implementation

uses exception;

fn i_encode(b : bytes) : istring
[
	return i_append~inline(0, b);
]

fn i_append(result : istring, b : bytes) : istring
[
	for i := 0 to len(b) do [
		var c := b[i];
		var code : byte;
		if c >= ' ' and c <= 126 then
			code := c;
		else
			abort internal("i_encode: invalid byte " + ntos(c));
		result := (result shl 7) + code;
	]
	return result;
]

fn i_decode(i : istring) : bytes
[
	var result := empty(byte);
	while i <> 0 do [
		var code := i and 127;
		if not (code >= ' ' and code <= 126) then
			abort internal("i_decode: invalid istring #" + ntos_base(i, 16));
		result := list(byte).[ code ] + result;
		i shr= 7;
	]
	return result;
]

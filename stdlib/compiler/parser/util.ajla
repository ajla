{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.parser.util;

uses pcode;

fn get_call_mode(l : list(bytes), fn_def : bool) : int;

implementation

fn get_call_mode(l : list(bytes), fn_def : bool) : int
[
	var call_mode := Call_Mode_Unspecified;
	for i := 0 to len(l) do [
		var s := l[i];
		if s = "normal" then call_mode := Call_Mode_Normal;
		else if s = "strict" then call_mode := Call_Mode_Strict;
		else if s = "spark" then call_mode := Call_Mode_Spark;
		else if s = "lazy" then call_mode := Call_Mode_Lazy;
		else if s = "inline" then call_mode := Call_Mode_Inline;
		else if s = "cache" then call_mode := Call_Mode_Cache;
		else if s = "save" then call_mode := Call_Mode_Save;
		else if s = "flat", fn_def then call_mode := Call_Mode_Flat;
	]
	return call_mode;
]

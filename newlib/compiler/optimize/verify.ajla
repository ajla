{*
 * Copyright (C) 2025 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.optimize.verify;

uses compiler.optimize.defs;

fn verify_function(ctx : context) : unit_type;

implementation

uses exception;
uses charset;
uses z3;
uses compiler.common.blob;
uses compiler.optimize.ssa;

fn allocate_variable(implicit z3w : z3_world, implicit z3ctx : z3_context, ctx : context, v : int, phi_node : int) : (z3_world, context, bytes);

fn get_z3_name(ctx : context, v : int, phi_node : int) : bytes
[
	var n := "var_";
	if phi_node > 0 then
		n += "phi_" + map(ntos_base(phi_node, 16), ascii_locase) + "_";
	n += map(ntos_base(v, 16), ascii_locase);
	if len_greater_than(ctx.variables[v].name, 0) then [
		n += "_" + ctx.variables[v].name;
	]
	return n;
]

fn get_z3_bb_name(ctx : context, bgi : int) : (context, bytes)
[
	var str := "bb_" + map(ntos_base(bgi, 16), ascii_locase);
	ctx.blocks[bgi].block_name := str;
	return ctx, str;
]

fn get_z3_type(ctx : context, runtime_type : int) : (context, bytes)
[
	if runtime_type = T_Bool then
		return ctx, "Bool";
	if runtime_type <= T_Integer, runtime_type >= T_Integer128 then
		return ctx, "Int";
	if runtime_type <= T_Real16, runtime_type >= T_Real128 then
		return ctx, "Real";
	if runtime_type = T_Undetermined then
		return ctx, "Undetermined";
	if runtime_type <= T_SInt8, runtime_type >= T_UInt128 then [
		var v := 8 shl ((T_SInt8 - runtime_type) shr 1);
		return ctx, "(_ BitVec " + ntos(v) + ")";
	]
	if runtime_type < 0 then
		return ctx, "";
	if len_greater_than(ctx.local_type_names[runtime_type], 0) then
		return ctx, ctx.local_type_names[runtime_type];
	var lt := ctx.local_types[runtime_type];
	if lt is arr or lt is flat_array then [
		var a : int;
		if lt is arr then
			a := lt.arr;
		else
			a := lt.flat_array.flat_type;
		var t1_str : bytes;
		ctx, t1_str := get_z3_type(ctx, a);
		if not len_greater_than(t1_str, 0) then
			return ctx, "";
		var str := "(Arr " + t1_str + ")";
		ctx.local_type_names[runtime_type] := str;
		return ctx, str;
	]
	if lt is flat_rec then [
		var str : bytes;
		ctx, str := get_z3_type(ctx, lt.flat_rec.non_flat_record);
		ctx.local_type_names[runtime_type] := str;
		return ctx, str;
	]
	return ctx, "";
]

fn get_array_type(ctx : context, v : int) : (context, bytes)
[
	var lt := ctx.local_types[ctx.variables[v].runtime_type];
	if lt is arr or lt is flat_array then [
		var a : int;
		if lt is arr then
			a := lt.arr;
		else
			a := lt.flat_array.flat_type;
		return get_z3_type(ctx, a);
	]
	abort internal("get_array_type: invalid type " + ntos(ord lt));
]

fn gen_assert(implicit z3w : z3_world, implicit z3ctx : z3_context, var_name : bytes, ops : bytes) : z3_world
[
	var str := "(assert (= " + var_name + " " + ops + "))";
	z3_eval_smtlib2_string_noret(str);
]

fn allocate_variable_block(implicit z3w : z3_world, implicit z3ctx : z3_context, ctx : context, v : int, phi_node : int, bb : int) : (z3_world, context, bytes)
[
	var ins := ctx.instrs[ctx.variables[v].defining_instr];
	if ins.bb = bb then [
		if ins.opcode = P_Phi, phi_node > 0 then
			return allocate_variable(ctx, ins.params[phi_node], 0);
		return allocate_variable(ctx, v, phi_node);
	] else [
		return allocate_variable(ctx, v, 0);
	]
]

fn declare_variable(implicit z3w : z3_world, implicit z3ctx : z3_context, ctx : context, n : bytes, lt : int) : (z3_world, context)
[
	var t : bytes;
	ctx, t := get_z3_type(ctx, lt);
	if len(t) = 0 then
		return ctx;
	z3_eval_smtlib2_string_noret("(declare-const " + n + " " + t + ")");
	if lt >= 0, ctx.local_types[lt] is rec then [
		if not ctx.local_types[lt].rec.opt then
			z3_eval_smtlib2_string_noret("(assert ((_ is mk-"+t+") "+n+"))");
	]
	return ctx;
]

fn assert_instruction(implicit z3w : z3_world, implicit z3ctx : z3_context, ctx : context, v : int, phi_node : int) : (z3_world, context)
[
	var var_name := ctx.variables[v].verifier_name[phi_node];
	var ins := ctx.instrs[ctx.variables[v].defining_instr];
	if ins.opcode = P_BinaryOp or ins.opcode = P_BinaryConstOp then [
		var t := ctx.variables[ins.params[3]].type_index;
		var is_bool := t = T_Bool;
		var is_int := t <= T_Integer and t >= T_Integer128;
		var is_real := t <= T_Real16 and t >= T_Real128;
		var op_z3 : bytes;
		var op := ins.params[0];
		if op = Bin_Add					then op_z3 := "+";
		else if op = Bin_Subtract			then op_z3 := "-";
		else if op = Bin_Multiply			then op_z3 := "*";
		else if op = Bin_Divide_Int			then op_z3 := "div";
		else if op = Bin_Divide_Real			then op_z3 := "/";
		else if op = Bin_Modulo				then op_z3 := "rem";
		else if op = Bin_And, is_bool			then op_z3 := "and";
		else if op = Bin_Or, is_bool			then op_z3 := "or";
		else if op = Bin_Xor, is_bool			then op_z3 := "xor";
		else if op = Bin_Equal				then op_z3 := "=";
		else if op = Bin_NotEqual			then op_z3 := "distinct";
		else if op = Bin_Less, is_int or is_real	then op_z3 := "<";
		else if op = Bin_LessEqual, is_int or is_real	then op_z3 := "<=";
		else if op = Bin_Greater, is_int or is_real	then op_z3 := ">";
		else if op = Bin_GreaterEqual, is_int or is_real then op_z3 := ">=";
		else if op = Bin_LessEqual, is_bool		then op_z3 := "=>";
		else return ctx;
		var var1 var1v var2 var2v : bytes;
		//eval debug("P_BinaryOp: " + ntos(ins.params[0]) + ", " + ntos(ins.params[1]) + ", " + ntos(ins.params[2]) + ", " + ntos(ins.params[3]) + ", " + ntos(ins.params[4]) + ", " + ntos(ins.params[5]));
		ctx, var1 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		if ins.opcode = P_BinaryOp then [
			ctx, var2 := allocate_variable_block(ctx, ins.params[5], phi_node, ins.bb);
			if len(var2) = 0 then
				return ctx;
			gen_assert(var_name, "(" + op_z3 + " " + var1 + " " + var2 + ")");
		] else [
			if is_bool then
				var2 := select(ins.params[4] <> 0, "false", "true");
			else
				var2 := ntos(ins.params[4]);
			gen_assert(var_name, "(" + op_z3 + " " + var1 + " " + var2 + ")");
		]
		return ctx;
	]
	if ins.opcode = P_UnaryOp then [
		var t := ctx.variables[v].type_index;
		var is_bool := t = T_Bool;
		var is_int := t <= T_Integer and t >= T_Integer128;
		var is_real := t <= T_Real16 and t >= T_Real128;
		var op_z3 : bytes;
		var op := ins.params[0];
		if op = Un_Not, is_bool				then op_z3 := "not";
		else if op = Un_Neg, is_int or is_real		then op_z3 := "-";
		else return ctx;
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		gen_assert(var_name, "(" + op_z3 + " " + var1 + ")");
		return ctx;
	]
	if ins.opcode = P_Copy then [
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		gen_assert(var_name, var1);
		return ctx;
	]
	if ins.opcode = P_Load_Const then [
		var cnst : bytes;
		var l := blob_to_int(ins.params[1 ..]);
		var t := ctx.variables[v].runtime_type;
		if t = T_Bool then [
			cnst := select(l <> 0, "false", "true");
		] else if t <= T_Integer and t >= T_Integer128 then [
			cnst := ntos(l);
		] else if t >= 0, ctx.local_types[t] is rec, ctx.local_types[t].rec.opt then [
			cnst := "mk-"+ctx.local_type_names[t]+"-"+ntos(l);
		] else [
			return ctx;
		]
		gen_assert(var_name, cnst);
		return ctx;
	]
	if ins.opcode = P_Structured_Write then [
		var n_steps := ins.params[0];
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		var idx := 6;
		var idxs := [ idx ];
		var old_var := var1;
		for i := 0 to n_steps - 1 do [
			var new_var := var_name + "-ld-" + ntos(i + 1);
			var stored_var := var_name + "-st-" + ntos(i + 1);
			if ins.params[idx] = Structured_Record then [
				var t1 : bytes;
				ctx, t1 := get_z3_type(ctx, ins.params[idx + 1]);
				if len(t1) = 0 then
					return ctx;
				var slot := ins.params[idx + 2];
				ctx := declare_variable(ctx, new_var, ctx.local_types[ins.params[idx + 1]].rec.types[slot]);
				ctx := declare_variable(ctx, stored_var, ctx.local_types[ins.params[idx + 1]].rec.types[slot]);
				var str := "(assert (= "+new_var+" (entry-"+t1+"-"+ntos(slot)+" "+old_var+")))";
				z3_eval_smtlib2_string_noret(str);
				idx += 3;
			] else if ins.params[idx] = Structured_Option then [
				var t1 : bytes;
				ctx, t1 := get_z3_type(ctx, ins.params[idx + 1]);
				if len(t1) = 0 then
					return ctx;
				var slot := ins.params[idx + 2];
				ctx := declare_variable(ctx, new_var, ctx.local_types[ins.params[idx + 1]].rec.types[slot]);
				ctx := declare_variable(ctx, stored_var, ctx.local_types[ins.params[idx + 1]].rec.types[slot]);
				var str := "(assert (= "+new_var+" (entry-"+t1+"-"+ntos(slot)+" "+old_var+")))";
				z3_eval_smtlib2_string_noret(str);
				idx += 3;
			] else if ins.params[idx] = Structured_Array then [
				var var2 : bytes;
				ctx, var2 := allocate_variable_block(ctx, ins.params[idx + 1], phi_node, ins.bb);
				if len(var2) = 0 then
					return ctx;
				ctx := declare_variable(ctx, new_var, ins.params[idx + 2]);
				ctx := declare_variable(ctx, stored_var, ins.params[idx + 2]);
				var str := "(assert (=> (and (>= "+var2+" 0) (< "+var2+" (arr-len "+old_var+"))) (= "+new_var+" (select (arr "+old_var+") "+var2+"))))";
				z3_eval_smtlib2_string_noret(str);
				idx += 3;
			] else [
				abort internal("assert_instruction: invalid structured type " + ntos(ins.params[idx]));
			]
			idxs +<= idx;
			old_var := new_var;
		]
		var svar : bytes;
		ctx, svar := allocate_variable_block(ctx, ins.params[5], phi_node, ins.bb);
		if len(svar) = 0 then
			return ctx;
		var i := n_steps - 1;
		while i >= 0 do [
			var new_var : bytes;
			if i = 0 then
				new_var := var_name;
			else
				new_var := var_name + "-st-" + ntos(i);
			var idx := idxs[i];
			if ins.params[idx] = Structured_Record then [
				var t1 : bytes;
				ctx, t1 := get_z3_type(ctx, ins.params[idx + 1]);
				if len(t1) = 0 then
					return ctx;
				var slot := ins.params[idx + 2];
				var str := "(assert (= "+new_var+" ((_ update-field entry-"+t1+"-"+ntos(slot)+") "+old_var+" "+svar+")))";
				z3_eval_smtlib2_string_noret(str);
			] else if ins.params[idx] = Structured_Option then [
				var t1 : bytes;
				ctx, t1 := get_z3_type(ctx, ins.params[idx + 1]);
				if len(t1) = 0 then
					return ctx;
				var slot := ins.params[idx + 2];
				var str := "(assert (= "+new_var+" (mk-"+t1+"-"+ntos(slot)+" "+svar+")))";
				z3_eval_smtlib2_string_noret(str);
			] else if ins.params[idx] = Structured_Array then [
				var var2 : bytes;
				ctx, var2 := allocate_variable_block(ctx, ins.params[idx + 1], phi_node, ins.bb);
				if len(var2) = 0 then
					return ctx;
				var str := "(assert (=> (and (>= "+var2+" 0) (< "+var2+" (arr-len "+old_var+"))) (= (arr "+new_var+") (store (arr "+old_var+") "+var2+" "+svar+"))))";
				z3_eval_smtlib2_string_noret(str);
				str := "(assert (=> (and (>= "+var2+" 0) (< "+var2+" (arr-len "+old_var+"))) (= (arr-len "+new_var+") (arr-len "+old_var+"))))";
				z3_eval_smtlib2_string_noret(str);
			] else [
				abort internal("assert_instruction: invalid structured type " + ntos(ins.params[idx]));
			]
			if i > 1 then
				old_var := var_name + "-ld-" + ntos(i - 1);
			else
				old_var := var1;
			svar := new_var;
			i -= 1;
		]
		return ctx;
	]
	if ins.opcode = P_Record_Create then [
		var lt := ctx.local_types[ctx.variables[v].runtime_type];
		var name := ctx.local_type_names[ctx.variables[v].runtime_type];
		if lt is flat_rec then [
			lt := ctx.local_types[lt.flat_rec.non_flat_record];
			name := ctx.local_type_names[lt.flat_rec.non_flat_record];
		]
		var str : bytes;
		if len(lt.rec.types) = 0 then [
			str := "mk-"+name;
		] else [
			str := "(mk-"+name;
			for i := 0 to len(lt.rec.types) do [
				var var1 : bytes;
				ctx, var1 := allocate_variable_block(ctx, ins.params[3 + i * 2], phi_node, ins.bb);
				if len(var1) = 0 then
					return ctx;
				str += " "+var1;
			]
			str += ")";
		]
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Record_Load then [
		var var1 t1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, t1 := get_z3_type(ctx, ctx.variables[ins.params[2]].runtime_type);
		if len(t1) = 0 then
			return ctx;
		var slot := ins.params[3];
		var str := "(entry-"+t1+"-"+ntos(slot)+" "+var1+")";
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Option_Create then [
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		var name := ctx.local_type_names[ctx.variables[v].runtime_type];
		var index := ins.params[1];
		var str := "(mk-"+name+"-"+ntos(index)+" "+var1+")";
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Option_Load then [
		var var1 t1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, t1 := get_z3_type(ctx, ctx.variables[ins.params[2]].runtime_type);
		if len(t1) = 0 then
			return ctx;
		var index := ins.params[3];
		var str := "(entry-"+t1+"-"+ntos(index)+" "+var1+")";
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Option_Test then [
		var t := ctx.variables[ins.params[1]].runtime_type;
		var lt := ctx.local_types[t];
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[1], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		var index := ins.params[2];
		var str : bytes;
		var b : bytes;
		ctx, b := get_z3_type(ctx, lt.rec.types[index]);
		//str := "(not (forall ((x "+b+")) (distinct "+var1+" (mk-"+ctx.local_type_names[t]+"-"+ntos(index)+" x))))";
		str := "((_ is mk-"+ctx.local_type_names[t]+"-"+ntos(index)+") "+var1+")";
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Option_Ord then [
		var t := ctx.variables[ins.params[1]].runtime_type;
		var lt := ctx.local_types[t];
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[1], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		var str := "";
		for i := 0 to len(lt.rec.types) do [
			str += "(ite ((_ is mk-"+ctx.local_type_names[t]+"-"+ntos(i)+") "+var1+") "+ntos(i)+" ";
		]
		str += "-1";
		str += fill(byte, ')', len(lt.rec.types));
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Array_Create then [
		var ln := ins.params[1];
		var t1 : bytes;
		ctx, t1 := get_array_type(ctx, v);
		if len(t1) = 0 then
			return ctx;
		var a := "(declare-const array-"+var_name+" (Array Int "+t1+"))";
		z3_eval_smtlib2_string_noret(a);
		var str := "array-"+var_name;
		for i := 0 to ln do [
			var var1 : bytes;
			ctx, var1 := allocate_variable_block(ctx, ins.params[4 + i * 2], phi_node, ins.bb);
			if len(var1) = 0 then
				return ctx;
			str := "(store "+str+" "+ntos(i)+" "+var1+")";
		]
		str := "(mk-arr "+ntos(ln)+" "+str+")";
		gen_assert(var_name, str);
		return ctx;
	]
	if ins.opcode = P_Array_Fill then [
		var var1 var2 t1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, t1 := get_z3_type(ctx, ctx.variables[ins.params[2]].runtime_type);
		if len(t1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		gen_assert(var_name, "(mk-arr "+var2+" ((as const (Array Int "+t1+")) "+var1+"))");
		return ctx;
	]
	if ins.opcode = P_Array_Load then [
		var var1 var2 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		var str := "(assert (=> (and (>= "+var2+" 0) (< "+var2+" (arr-len "+var1+"))) (= "+var_name+" (select (arr "+var1+") "+var2+"))))";
		z3_eval_smtlib2_string_noret(str);
		return ctx;
	]
	if ins.opcode = P_Array_Len then [
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[1], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		gen_assert(var_name, "(arr-len "+var1+")");
		return ctx;
	]
	if ins.opcode = P_Array_Len_Greater_Than then [
		var var1 var2 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[1], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		gen_assert(var_name, "(> (arr-len "+var1+") "+var2+")");
		return ctx;
	]
	if ins.opcode = P_Array_Sub or ins.opcode = P_Array_Skip then [
		var var1 var2 var3 t1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[3], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		if ins.opcode = P_Array_Sub then [
			ctx, var3 := allocate_variable_block(ctx, ins.params[4], phi_node, ins.bb);
			if len(var3) = 0 then
				return ctx;
		] else [
			var3 := "(arr-len "+var1+")";
		]
		var str := "(assert (=> (and (<= 0 "+var2+") (<= "+var2+" "+var3+") (<= "+var3+" (arr-len "+var1+"))) (and (= (arr-len "+var_name+") (- "+var3+" "+var2+")) (forall ((i Int)) (=> (and (>= i "+var2+") (< i "+var3+")) (= (select (arr "+var1+") i) (select (arr "+var_name+") (- i "+var2+"))))))))";
		z3_eval_smtlib2_string_noret(str);
		return ctx;
	]
	if ins.opcode = P_Array_Append then [
		var var1 var2 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[4], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		var str := "(assert (= (arr-len "+var_name+") (+ (arr-len "+var1+") (arr-len "+var2+"))))";
		z3_eval_smtlib2_string_noret(str);
		str := "(assert (forall ((i Int)) (=> (and (>= i 0) (< i (arr-len "+var1+"))) (= (select (arr "+var1+") i) (select (arr "+var_name+") i)))))";
		z3_eval_smtlib2_string_noret(str);
		str := "(assert (forall ((i Int)) (=> (and (>= i 0) (< i (arr-len "+var2+"))) (= (select (arr "+var2+") i) (select (arr "+var_name+") (+ i (arr-len "+var1+")))))))";
		z3_eval_smtlib2_string_noret(str);
		return ctx;
	]
	if ins.opcode = P_Array_Append_One then [
		var var1 var2 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		ctx, var2 := allocate_variable_block(ctx, ins.params[4], phi_node, ins.bb);
		if len(var2) = 0 then
			return ctx;
		var str := "(assert (= (arr-len "+var_name+") (+ (arr-len "+var1+") 1)))";
		z3_eval_smtlib2_string_noret(str);
		str := "(assert (= (arr "+var_name+") (store (arr "+var1+") (arr-len "+var1+") "+var2+")))";
		z3_eval_smtlib2_string_noret(str);
		return ctx;
	]
	if ins.opcode = P_Array_Flatten then [
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, ins.params[2], phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		gen_assert(var_name, var1);
		return ctx;
	]
	if ins.opcode = P_Return_Vars then [
		var new_v : int;
		for i := 0 to len(ins.params) do [
			if ins.params[i] = v then [
				new_v := ctx.return_ins.params[1 + 2 * i];
				goto found_new_v;
			]
		]
		abort internal("P_Return_Vars parameter not found");
found_new_v:
		var var1 : bytes;
		ctx, var1 := allocate_variable_block(ctx, new_v, phi_node, ins.bb);
		if len(var1) = 0 then
			return ctx;
		gen_assert(var_name, var1);
		return ctx;
	]
	//eval debug("unknown opcode: " + ntos(ins.opcode) + " (" + ctx.variables[v].verifier_name[0] + ")");
	return ctx;
]

fn allocate_variable(implicit z3w : z3_world, implicit z3ctx : z3_context, ctx : context, v : int, phi_node : int) : (z3_world, context, bytes)
[
	if not len_greater_than(ctx.variables[v].verifier_name[phi_node], 0) then [
		var n := get_z3_name(ctx, v, phi_node);
		ctx.variables[v].verifier_name[phi_node] := n;
		ctx := declare_variable(ctx, n, ctx.variables[v].runtime_type);
		ctx := assert_instruction(ctx, v, phi_node);
	]
	return ctx, ctx.variables[v].verifier_name[phi_node];
]

fn verify_function(ctx : context) : unit_type
[
	var b : bytes;
	implicit var z3w := z3_mk_world;
	implicit var z3ctx := z3_mk_context();
	//eval debug("verify function " + ctx.name);

	z3_eval_smtlib2_string_noret("(declare-sort Undetermined 0)");
	z3_eval_smtlib2_string_noret("(declare-datatypes (T) ((Arr (mk-arr (arr-len Int) (arr (Array Int T))))))");

	var str := "(declare-datatypes (";
	var first := true;
	for lti := 0 to len(ctx.local_types) do [
		var lt := ctx.local_types[lti];
		if lt is arr or lt is flat_array or lt is flat_rec then
			continue;
		if not first then
			str += " ";
		first := false;
		var name := "LT_" + ntos(lti) + "-" + lt.rec.name;
		ctx.local_type_names[lti] := name;
		str += "(" + name + " 0)";
		//eval debug("NAME: " + name);
	]
	str += ") (";
	first := true;
	for lti := 0 to len(ctx.local_types) do [
		var lt := ctx.local_types[lti];
		if lt is arr or lt is flat_array or lt is flat_rec then
			continue;
		if not first then
			str += " ";
		first := false;
		var ltr := lt.rec;
		var name := ctx.local_type_names[lti];
		if not ltr.opt then [
			str += "(" + name + " (mk-" + name;
			for i := 0 to len(ltr.types) do [
				if ltr.types[i] = T_Type then
					continue;
				ctx, b := get_z3_type(ctx, ltr.types[i]);
				str += " (entry-" + name + "-" + ntos(i) + " " + b + ")";
			]
			str += "))";
		] else [
			str += "(" + name;
			for i := 0 to len(ltr.types) do [
				str += " (mk-" + name + "-" + ntos(i);
				if ltr.types[i] <> T_EmptyOption then [
					//eval debug("OPT: " + ntos(ltr.types[i]));
					ctx, b := get_z3_type(ctx, ltr.types[i]);
					str += " (entry-" + name + "-" + ntos(i) + " " + b + ")";
				]
				str += ")";
			]
			str += ")";
		]
	]
	str += "))";
	z3_eval_smtlib2_string_noret(str);

	for bgi := 0 to len(ctx.blocks) do [
		if not ctx.blocks[bgi].active then
			continue;
		if len(ctx.blocks[bgi].post_list) = 3 then
			ctx := deactivate_arrow(ctx, bgi, 2);
	]
	ctx := prune_unreachable(ctx);

	for bgi := 0 to len(ctx.blocks) do [
		if not ctx.blocks[bgi].active then
			continue;
		for ili := 0 to len(ctx.blocks[bgi].instrs) do [
			var igi := ctx.blocks[bgi].instrs[ili];
			var ins := ctx.instrs[igi];
			if ins.opcode = P_Return then [
				ctx.return_ins := ins;
			]
		]
		for i := 0 to len(ctx.blocks[bgi].post_list) do [
			var p := ctx.blocks[bgi].post_list[i];
			// TODO: this doesn't eliminate all loops
			if ctx.blocks[p].dominates bt bgi then [
				ctx.blocks[bgi].post_cut_nodes bts= i;
				ctx.blocks[p].pre_cut_nodes := true;
			]
		]
		var str : bytes;
		ctx, str := get_z3_bb_name(ctx, bgi);
		z3_eval_smtlib2_string_noret("(declare-const " + str + " Bool)");
	]

	for bgi := 0 to len(ctx.blocks) do [
		if not ctx.blocks[bgi].active then
			continue;
		for ili := 0 to len(ctx.blocks[bgi].instrs) do [
			var igi := ctx.blocks[bgi].instrs[ili];
			var ins := ctx.instrs[igi];
			if ins.opcode = P_Invariant then [
				if not len_greater_than(ctx.blocks[bgi].pred_list, 0) then [
					var str := "A block with invariant statement has not predecessor in " + ctx.name;
					return exception_make_str(unit_type, ec_async, error_compiler_error, 0, false, str);

				]
				for i := 0 to len(ctx.blocks[bgi].pred_list) do [
					var var1 : bytes;
					ctx, var1 := allocate_variable(ctx, ins.params[0], i + 1);
					if len(var1) = 0 then
						var1 := "false";
					var new_blk := ctx.blocks[bgi].pred_list[i];
					ctx.blocks[new_blk].added_claims +<= var1;
				]
			]
		]
	]

	for bgi := 0 to len(ctx.blocks) do [
		if not ctx.blocks[bgi].active then
			continue;
		var assumes := "";
		var claims := "";
		var guards := "";
		var all_phis := "";
		for i := 0 to len(ctx.blocks[bgi].pred_list) do [
			var p := ctx.blocks[bgi].pred_list[i];
			var guard : bytes;
			if len_greater_than(ctx.blocks[p].instrs, 0) then [
				var ins := ctx.instrs[ctx.blocks[p].instrs[len(ctx.blocks[p].instrs) - 1]];
				if ins.opcode = P_Jmp_False then [
					var pos := ctx.blocks[bgi].pred_position[i];
					var var1 : bytes;
					ctx, var1 := allocate_variable(ctx, ins.params[0], 0);
					if pos = 0 then
						guard := var1;
					else if pos = 1 then
						guard := "(not " + var1 + ")";
					else
						guard := "false";
				] else [
					guard := "true";
				]
			] else [
				guard := "true";
			]
			guards += " " + guard;
			if not ctx.blocks[bgi].pre_cut_nodes then [
				var phis := "";
				for ili := 0 to len(ctx.blocks[bgi].instrs) do [
					var igi := ctx.blocks[bgi].instrs[ili];
					var ins := ctx.instrs[igi];
					if ins.opcode <> P_Phi then
						break;
					var phi1 var1 : bytes;
					ctx, phi1 := allocate_variable(ctx, ins.params[0], 0);
					ctx, var1 := allocate_variable(ctx, ins.params[i + 1], 0);
					phis += " (= " + phi1 + " " + var1 + ")";
				]
				if len_greater_than(phis, 0) then [
					all_phis += " (and" + phis + ")";
				]
			]
		]
		if len_greater_than(all_phis, 0) then
			assumes += " (or" + all_phis + ")";
		if len_greater_than(guards, 0) then
			assumes += " (or" + guards + ")";
		for ili := 0 to len(ctx.blocks[bgi].instrs) do [
			var igi := ctx.blocks[bgi].instrs[ili];
			var ins := ctx.instrs[igi];
			if ins.opcode = P_Assume or ins.opcode = P_Invariant then [
				var var1 : bytes;
				ctx, var1 := allocate_variable(ctx, ins.params[0], 0);
				if len_greater_than(var1, 0) then
					assumes += " " + var1;
			] else if ins.opcode = P_Claim then [
				var var1 : bytes;
				ctx, var1 := allocate_variable(ctx, ins.params[0], 0);
				if len_greater_than(var1, 0) then [
					claims += " " + var1;
				]
			]
		]
		for i := 0 to len(ctx.blocks[bgi].added_claims) do [
			claims += " " + ctx.blocks[bgi].added_claims[i];
		]
		for i := 0 to len(ctx.blocks[bgi].post_list) do [
			if ctx.blocks[bgi].post_cut_nodes bt i then
				continue;
			var p := ctx.blocks[bgi].post_list[i];
			claims += " " + ctx.blocks[p].block_name;
		]
		if len(assumes) = 0 then
			assumes := " true";
		if len(claims) = 0 then
			claims := " true";
		z3_eval_smtlib2_string_noret("(assert (= " + ctx.blocks[bgi].block_name + " (=> (and" + assumes + ") (and" + claims + "))))");
	]

	z3_eval_smtlib2_string_noret("(assert (not " + ctx.blocks[0].block_name + "))");
	b := z3_eval_smtlib2_string("(check-sat)");
	if list_begins_with(b, "unsat") then
		return unit_value;
	if list_begins_with(b, "sat") then [
		b := z3_eval_smtlib2_string("(get-model)");
		b := "Verification of function " + ctx.name + " failed:" + nl + b;
		return exception_make_str(unit_type, ec_async, error_compiler_error, 0, false, b);
	]
	b := "Verification of function " + ctx.name + " inconclusive";
	return exception_make_str(unit_type, ec_async, error_compiler_error, 0, false, b);
]

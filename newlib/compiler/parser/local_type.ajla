{*
 * Copyright (C) 2024, 2025 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.parser.local_type;

uses compiler.parser.dict;
uses compiler.parser.type;

fn get_record_option_type_cc(ctx : function_context, cc : compare_context, typ_cc : compare_context, typ_idx : int) : (function_context, int);
fn get_array_type_cc(ctx : function_context, cc : compare_context, typ : int) : (function_context, int);
fn get_type_cc(ctx : function_context, cc : compare_context, typ : int) : (function_context, int);
fn generate_local_types(ctx : function_context) : function_context;

implementation

uses compiler.common.gvn;
uses compiler.parser.gen2;

fn local_type_idx(ctx : function_context, lt : local_type) : (function_context, int)
[
	for i := 0 to len(ctx.local_types) do [
		var match := verify_type_equality(ctx, ctx.local_types[i].cc, ctx.local_types[i].typ, empty(compare_fn_stack), lt.cc, lt.typ, empty(compare_fn_stack));
		if match is eq then
			return ctx, i;
	]
	var idx := len(ctx.local_types);
	ctx.local_types +<= lt;
	return ctx, idx;
]

fn get_record_option_type_cc(ctx : function_context, cc : compare_context, typ_cc : compare_context, typ_idx : int) : (function_context, int)
[
	var rec_ctx := cc.ctx;
	var lt := local_type.[
		mode : select(rec_ctx.record_def.j.is_option, Local_Type_Record, Local_Type_Option),
		args : generate_function_id(rec_ctx.id),
		cc : typ_cc,
		typ : typ_idx,
	];
	return local_type_idx(ctx, lt);
]

fn get_array_type_cc(ctx : function_context, cc : compare_context, typ : int) : (function_context, int)
[
	cc, typ := get_base_definition(ctx, cc, typ, false);
	if typ < 0 and typ <> T_UnknownType then
		return ctx, typ;
	var list_cc, list_typ := list_type(ctx, cc, typ, false, empty(token));
	if list_typ <> T_InvalidType then [
		//return ctx, T_Undetermined;
		var sub_lt : int;
		ctx, sub_lt := get_type_cc(ctx, list_cc, list_typ);
		var lt := local_type.[
			mode : Local_Type_Array,
			args : list(pcode_t).[ sub_lt ],
			cc : cc,
			typ : typ,
		];
		return local_type_idx(ctx, lt);
	]
	return ctx, T_Undetermined;
]

fn get_type_cc(ctx : function_context, cc : compare_context, typ : int) : (function_context, int)
[
	var content_cc, content_typ := get_base_definition(ctx, cc, typ, true);
	if content_typ = T_Record then [
		return get_record_option_type_cc(ctx, content_cc, cc, typ);
	] else [
		return get_array_type_cc(ctx, cc, typ);
	]
]

fn get_local_type(ctx : function_context, i : int) : (function_context, int)
[
	var cc, typ := get_deep_type_of_var(ctx, i);
	if typ = T_Record then [
		var cc1 := new_compare_context(ctx);
		return get_record_option_type_cc(ctx, cc, cc1, get_type_of_var(ctx, i));
	]
	return get_array_type_cc(ctx, cc, typ);
]

fn generate_local_types(ctx : function_context) : function_context
[
	for i := 0 to len(ctx.variables) do [
		var lt : int;
		ctx, lt := get_local_type(ctx, i);
		ctx.variables[i].local_type := lt;
	]
	var i := 0;
	while i < len(ctx.local_types) do [
		var lt := ctx.local_types[i];
		if lt.mode = Local_Type_Record then [
			var rec_cc, rec_typ := get_base_definition(ctx, lt.cc, lt.typ, true);
			if rec_typ <> T_Record then
				abort internal("generate_local_types: expecting record");
			var rec := rec_cc.ctx.record_def.j;
			var l := empty(pcode_t);
			var cc, types := generate_record_entry_types(ctx, rec_cc);
			for j := 0 to len(types) do [
				var lt_idx : int;
				ctx, lt_idx := get_type_cc(ctx, cc, types[j]);
				l +<= lt_idx;
			]
			ctx.local_types[i].args +<= len(l);
			ctx.local_types[i].args += l;
		] else if lt.mode = Local_Type_Option then [
			var rec_cc, rec_typ := get_base_definition(ctx, lt.cc, lt.typ, true);
			if rec_typ <> T_Record then
				abort internal("generate_local_types: expecting record");
			var rec := rec_cc.ctx.record_def.j;
			var l := empty(pcode_t);
			for j := 0 to len(rec.entries) do [
				var lt_idx : int;
				ctx, lt_idx := get_type_cc(ctx, rec_cc, rec.entries[j].type_idx);
				l +<= lt_idx;
			]
			ctx.local_types[i].args +<= len(l);
			ctx.local_types[i].args += l;
		]
		i += 1;
	]
	return ctx;
]

{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit ui.widget.text;

uses ui.widget.common;

type text_state;

fn text_init(align : widget_align, label : string, color_scheme : bytes, w : world, app : appstate, id : wid) : (world, appstate, text_state);
fn text_get_width(app : appstate, com : widget_common, st : text_state, x : int) : int;
fn text_get_height(app : appstate, com : widget_common, st : text_state, x : int) : int;
fn text_reflow(app : appstate, com : widget_common, st : text_state) : (appstate, widget_common, text_state);
fn text_redraw(app : appstate, curs : curses, com : widget_common, st : text_state) : curses;

const text_class~flat := widget_class.[
	t : text_state,
	name : "text",
	is_selectable : false,
	get_width : text_get_width,
	get_height : text_get_height,
	reflow : text_reflow,
	redraw : text_redraw,
];

implementation

record text_state [
	align : widget_align;
	label : string;
	color_scheme : bytes;
	flow : list(string);
]

fn text_init(align : widget_align, label : string, color_scheme : bytes, implicit w : world, implicit app : appstate, id : wid) : (world, appstate, text_state)
[
	return text_state.[
		align : align,
		label : label,
		color_scheme : color_scheme,
		flow : [ label ],
	];
]

fn reflow(label : string, x : int, hard_break : bool) : list(string)
[
	var lbr := -1;
	var tmp_line := empty(char);
	var tmp_len := 0;
	var result := empty(string);
	for i := 0 to len(label) do [
		var c := label[i];
		if c = u_nl then [
			result +<= tmp_line;
			tmp_line := empty(char);
			tmp_len := 0;
			lbr := -1;
			continue;
		]
		if c = ' ' then
			lbr := len(tmp_line);
		tmp_line +<= c;
		tmp_len += char_length(c);
		if tmp_len > x then [
			if lbr >= 0 then [
				result +<= tmp_line[ .. lbr];
				tmp_line := tmp_line[lbr + 1 .. ];
			] else if hard_break, tmp_len > 1 then [
				result +<= tmp_line[ .. tmp_len - 1];
				tmp_line := tmp_line[tmp_len - 1 .. ];
			]
			tmp_len := string_length(tmp_line);
			lbr := -1;
		]
	]
	if tmp_len > 0 then
		result +<= tmp_line;
	return result;
]

fn text_get_width(app : appstate, com : widget_common, st : text_state, x : int) : int
[
	var flow := reflow(st.label, x, false);
	var x := 0;
	for i := 0 to len(flow) do
		x := max(x, len(flow[i]));
	return x;
]

fn text_get_height(app : appstate, com : widget_common, st : text_state, x : int) : int
[
	var flow := reflow(st.label, x, true);
	return len(flow);
]

fn text_reflow(implicit app : appstate, implicit com : widget_common, implicit st : text_state) : (appstate, widget_common, text_state)
[
	var flow := reflow(st.label, com.size_x, true);
	st.flow := flow;
]

fn text_redraw(implicit app : appstate, implicit curs : curses, com : widget_common, st : text_state) : curses
[
	property_set_attrib(property_get_attrib(st.color_scheme + "text", #0000, #0000, #0000, #aaaa, #aaaa, #aaaa, 0, curses_invert));
	curses_fill_rect(0, com.size_x, 0, com.size_y, ' ');
	for i := 0 to len(st.flow) do [
		var s := st.flow[i];
		var x := 0;
		if st.align is center then
			x := com.size_x - string_length(s) shr 1;
		else if st.align is right then
			x := com.size_x - string_length(s);
		curses_set_pos(x, i);
		curses_print(s);
	]
]

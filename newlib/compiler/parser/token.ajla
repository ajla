{*
 * Copyright (C) 2024, 2025 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.parser.token;

uses compiler.parser.istring;

option token_opt [
	identifier : istring;
	macro_expansion : istring;
	oper : istring;
	number : bytes;
	character : bytes;
	string : bytes;
	unicode : bytes;
	k_unit;
	k_implementation;
	k_uses;
	k_fn;
	k_lambda;
	k_const;
	k_mutable;
	k_operator;
	k_prefix;
	k_postfix;
	k_type;
	k_sint8;
	k_uint8;
	k_sint16;
	k_uint16;
	k_sint32;
	k_uint32;
	k_sint64;
	k_uint64;
	k_sint128;
	k_uint128;
	k_int;
	k_int8;
	k_int16;
	k_int32;
	k_int64;
	k_int128;
	k_real16;
	k_real32;
	k_real64;
	k_real80;
	k_real128;
	k_bool;
	k_false;
	k_true;
	k_record;
	k_option;
	k_private;
	k_implicit;
	k_conversion;
	k_prereq;
	k_contract;
	k_var;
	k_return;
	k_if;
	k_then;
	k_else;
	k_while;
	k_do;
	k_for;
	k_to;
	k_in;
	k_break;
	k_continue;
	k_goto;
	k_eval;
	k_xeval;
	k_abort;
	k_keep;
	k_assume;
	k_claim;
	k_invariant;
	k_is;
	k_ord;
	k_define;
	comma;
	semicolon;
	dot;
	range;
	colon;
	assign;
	left_paren;
	right_paren;
	left_bracket;
	right_bracket;
	internal_variable : int;
	invalid;
]

record token [
	t : token_opt;
	prev_hints : list(bytes);
	post_hints : list(bytes);
	bracket_skip : int;
	file : bytes;
	line : int;
	column : int;
]

type tokens := list(token);

fn token_location(t : token) : bytes;
fn compiler_error(s : bytes, t : tokens) : unit_type;
fn tokenize_string(str : bytes, t : tokens) : tokens;
fn tokenize(str : bytes, filename : bytes) : tokens;
fn token_opt_to_string(t : token_opt, us : bool) : bytes;
fn token_to_string(t : token) : bytes;
fn tokens_to_string(t : tokens) : bytes;
fn token_find(tl : tokens, t : token_opt, err : bool) : int;
fn token_find_3(tl : tokens, t1 t2 t3 : token_opt, err : bool) : int;
fn token_find_colon(t : tokens) : int;
fn split_tokens(tl : tokens, t : token_opt) : (tokens, tokens);
fn token_assert(tl : tokens, t : token_opt) : unit_type;
fn new_token(t : token, opt : token_opt) : token;

implementation

uses exception;

fn token_opt_to_string(t : token_opt, us : bool) : bytes
[
	var str : bytes;
	var stri := "";
	if t is identifier then [
		str := "identifier";
		stri := i_decode(t.identifier);
	] else if t is macro_expansion then [
		str := "macro expansion";
		stri := i_decode(t.macro_expansion);
	] else if t is oper then [
		str := "operator";
		stri := i_decode(t.oper);
	] else if t is number then [
		str := "number";
		stri := t.number;
	] else if t is character then [
		str := "character";
		stri := t.character;
	] else if t is string then [
		str := "string";
		stri := t.string;
	] else if t is unicode then [
		str := "unicode";
		stri := t.unicode;
	]
	else if t is k_unit then str := "unit";
	else if t is k_implementation then str := "implementation";
	else if t is k_uses then str := "uses";
	else if t is k_fn then str := "fn";
	else if t is k_lambda then str := "lambda";
	else if t is k_const then str := "const";
	else if t is k_mutable then str := "mutable";
	else if t is k_operator then str := "oper";
	else if t is k_prefix then str := "prefix";
	else if t is k_postfix then str := "postfix";
	else if t is k_type then str := "type";
	else if t is k_sint8 then str := "sint8";
	else if t is k_uint8 then str := "uint8";
	else if t is k_sint16 then str := "sint16";
	else if t is k_uint16 then str := "uint16";
	else if t is k_sint32 then str := "sint32";
	else if t is k_uint32 then str := "uint32";
	else if t is k_sint64 then str := "sint64";
	else if t is k_uint64 then str := "uint64";
	else if t is k_sint128 then str := "sint128";
	else if t is k_uint128 then str := "uint128";
	else if t is k_int then str := "int";
	else if t is k_int8 then str := "int8";
	else if t is k_int16 then str := "int16";
	else if t is k_int32 then str := "int32";
	else if t is k_int64 then str := "int64";
	else if t is k_int128 then str := "int128";
	else if t is k_real16 then str := "real16";
	else if t is k_real32 then str := "real32";
	else if t is k_real64 then str := "real64";
	else if t is k_real80 then str := "real80";
	else if t is k_real128 then str := "real128";
	else if t is k_bool then str := "bool";
	else if t is k_false then str := "false";
	else if t is k_true then str := "true";
	else if t is k_record then str := "record";
	else if t is k_option then str := "option";
	else if t is k_private then str := "private";
	else if t is k_implicit then str := "implicit";
	else if t is k_conversion then str := "conversion";
	else if t is k_prereq then str := "prereq";
	else if t is k_contract then str := "contract";
	else if t is k_var then str := "var";
	else if t is k_return then str := "return";
	else if t is k_if then str := "if";
	else if t is k_then then str := "then";
	else if t is k_else then str := "else";
	else if t is k_while then str := "while";
	else if t is k_do then str := "do";
	else if t is k_for then str := "for";
	else if t is k_to then str := "to";
	else if t is k_in then str := "in";
	else if t is k_break then str := "break";
	else if t is k_continue then str := "continue";
	else if t is k_goto then str := "goto";
	else if t is k_eval then str := "eval";
	else if t is k_xeval then str := "xeval";
	else if t is k_abort then str := "abort";
	else if t is k_keep then str := "keep";
	else if t is k_assume then str := "assume";
	else if t is k_claim then str := "claim";
	else if t is k_invariant then str := "invariant";
	else if t is k_is then str := "is";
	else if t is k_ord then str := "ord";
	else if t is k_define then str := "define";
	else if t is comma then str := ",";
	else if t is semicolon then str := ";";
	else if t is dot then str := ".";
	else if t is range then str := "..";
	else if t is colon then str := ":";
	else if t is assign then str := ":=";
	else if t is left_paren then str := "(";
	else if t is right_paren then str := ")";
	else if t is left_bracket then str := "[";
	else if t is right_bracket then str := "]";
	else if t is internal_variable then [
		str := "variable";
		stri := ntos(t.internal_variable);
	]
	else if t is invalid then return "Invalid";
	else return "Unknown";

	if len(stri) > 0 then
		if us then
			str := stri;
		else
			str += " " + stri;

	return str;
]

fn token_to_string(t : token) : bytes
[
	var s := token_opt_to_string(t.t, false);
	if t.bracket_skip <> 0 then
		s += " (" + ntos(t.bracket_skip) + ")";
	if len(t.prev_hints) > 0 then [
		s += " prev:";
		for i := 0 to len(t.prev_hints) do [
			s +<= '~';
			s += t.prev_hints[i];
		]
	]
	if len(t.post_hints) > 0 then [
		s += " post:";
		for i := 0 to len(t.post_hints) do [
			s +<= '~';
			s += t.post_hints[i];
		]
	]
	return s;
]

fn tokens_to_string(t : tokens) : bytes
[
	var result := "";
	for i := 0 to len(t) do [
		if i > 0 then
			result += "  ";
		result += token_to_string(t[i]);
	]
	return result;
]

fn get_kwmap~cache : list(token_opt)
[
	var result := infinite_uninitialized(token_opt);
	result[i_encode("unit")] := token_opt.k_unit;
	result[i_encode("implementation")] := token_opt.k_implementation;
	result[i_encode("uses")] := token_opt.k_uses;
	result[i_encode("fn")] := token_opt.k_fn;
	result[i_encode("lambda")] := token_opt.k_lambda;
	result[i_encode("const")] := token_opt.k_const;
	result[i_encode("mutable")] := token_opt.k_mutable;
	result[i_encode("operator")] := token_opt.k_operator;
	result[i_encode("prefix")] := token_opt.k_prefix;
	result[i_encode("postfix")] := token_opt.k_postfix;
	result[i_encode("type")] := token_opt.k_type;
	result[i_encode("sint8")] := token_opt.k_sint8;
	result[i_encode("uint8")] := token_opt.k_uint8;
	result[i_encode("sint16")] := token_opt.k_sint16;
	result[i_encode("uint16")] := token_opt.k_uint16;
	result[i_encode("sint32")] := token_opt.k_sint32;
	result[i_encode("uint32")] := token_opt.k_uint32;
	result[i_encode("sint64")] := token_opt.k_sint64;
	result[i_encode("uint64")] := token_opt.k_uint64;
	result[i_encode("sint128")] := token_opt.k_sint128;
	result[i_encode("uint128")] := token_opt.k_uint128;
	result[i_encode("int")] := token_opt.k_int;
	result[i_encode("int8")] := token_opt.k_int8;
	result[i_encode("int16")] := token_opt.k_int16;
	result[i_encode("int32")] := token_opt.k_int32;
	result[i_encode("int64")] := token_opt.k_int64;
	result[i_encode("int128")] := token_opt.k_int128;
	result[i_encode("real16")] := token_opt.k_real16;
	result[i_encode("real32")] := token_opt.k_real32;
	result[i_encode("real64")] := token_opt.k_real64;
	result[i_encode("real80")] := token_opt.k_real80;
	result[i_encode("real128")] := token_opt.k_real128;
	result[i_encode("bool")] := token_opt.k_bool;
	result[i_encode("false")] := token_opt.k_false;
	result[i_encode("true")] := token_opt.k_true;
	result[i_encode("record")] := token_opt.k_record;
	result[i_encode("option")] := token_opt.k_option;
	result[i_encode("private")] := token_opt.k_private;
	result[i_encode("implicit")] := token_opt.k_implicit;
	result[i_encode("conversion")] := token_opt.k_conversion;
	result[i_encode("prereq")] := token_opt.k_prereq;
	result[i_encode("contract")] := token_opt.k_contract;
	result[i_encode("var")] := token_opt.k_var;
	result[i_encode("return")] := token_opt.k_return;
	result[i_encode("if")] := token_opt.k_if;
	result[i_encode("then")] := token_opt.k_then;
	result[i_encode("else")] := token_opt.k_else;
	result[i_encode("while")] := token_opt.k_while;
	result[i_encode("do")] := token_opt.k_do;
	result[i_encode("for")] := token_opt.k_for;
	result[i_encode("to")] := token_opt.k_to;
	result[i_encode("in")] := token_opt.k_in;
	result[i_encode("break")] := token_opt.k_break;
	result[i_encode("continue")] := token_opt.k_continue;
	result[i_encode("goto")] := token_opt.k_goto;
	result[i_encode("eval")] := token_opt.k_eval;
	result[i_encode("xeval")] := token_opt.k_xeval;
	result[i_encode("abort")] := token_opt.k_abort;
	result[i_encode("keep")] := token_opt.k_keep;
	result[i_encode("assume")] := token_opt.k_assume;
	result[i_encode("claim")] := token_opt.k_claim;
	result[i_encode("invariant")] := token_opt.k_invariant;
	result[i_encode("is")] := token_opt.k_is;
	result[i_encode("ord")] := token_opt.k_ord;
	result[i_encode("define")] := token_opt.k_define;
	return result;
]

fn get_opmap~cache : list(token_opt)
[
	var result := infinite_uninitialized(token_opt);
	result[i_encode(".")] := token_opt.dot;
	result[i_encode("..")] := token_opt.range;
	result[i_encode(":")] := token_opt.colon;
	result[i_encode(":=")] := token_opt.assign;
	return result;
]

fn token_location(t : token) : bytes
[
	return t.file + ":" + ntos(t.line) + ":" + ntos(t.column) + ": ";
]

fn compiler_error(s : bytes, t : tokens) : unit_type
[
	if len(t) > 0 then
		s := token_location(t[0]) + s;
	return exception_make_str(unit_type, ec_async, error_compiler_error, 0, false, s);
]

fn compiler_error_tk(s : bytes, tk : token) : unit_type
[
	s := token_location(tk) + s;
	return exception_make_str(unit_type, ec_async, error_compiler_error, 0, false, s);
]

fn rng~inline(start end : int128) : int128
[
	var one : int128 := 1;
	return (one shl end + 1) - (one shl start);
]

fn p2~inline(i : int128) : int128
[
	var one : int128 := 1;
	return one shl i;
]

const white_bitmap : int128 := p2(9) + p2(10) + p2(13) + p2(' ');
const num_start_bitmap : int128 := rng('0', '9') + p2('#');
const num_middle_bitmap : int128 := rng('0', '9');
const num_middle_hex_bitmap : int128 := rng('0', '9') + rng('A', 'F') + rng('a', 'f');
const ident_start_bitmap : int128 := rng('A', 'Z') + rng('a', 'z') + p2('@');
const ident_middle_bitmap : int128 := rng('0', '9') + rng('A', 'Z') + rng('a', 'z') + p2('_') + p2('@');
const operator_bitmap : int128 :=
	p2('!') + p2('$') + p2('%') + p2('&') + p2('*') + p2('+') + p2('-') +
	p2('.') + p2('/') + p2(':') + p2('<') + p2('=') + p2('>') + p2('?') +
	p2('\') + p2('^') + p2('|');

{ " # ' ( ) , : ; @ [ ] { } ~ }

fn tokenize_recursive(str : bytes, filename : bytes, line : int, column : int, kwmap : list(token_opt), opmap : list(token_opt)) : tokens
[
	var i := 0;
	var line_start := -1;
	var result := empty(token);

	var bracket_stack_pos := empty(int);
	var bracket_stack_type := empty(byte);
	var next_hints := empty(bytes);

	//for q := 0 to 1000000000 do ;

	while len_greater_than(str, i) do [
		var start := i;
		var tk := token.[ t : token_opt.invalid, prev_hints : next_hints, post_hints : empty(bytes), bracket_skip : 0, file : filename, line : line, column : i - line_start ];
		var c := str[i];
		if white_bitmap bt c then [
			if c = 10 then [
				line_start := i;
				line += 1;
			]
			i += 1;
		] else if num_start_bitmap bt c then [
			var mid := num_middle_bitmap;
			var exp := 'E';
			var hex := false;
			if c = '#' then [
				exp := 'P';
				hex := true;
				mid := num_middle_hex_bitmap;
				i += 1;
				if not len_greater_than(str, i) then [
invalid_number:
					abort compiler_error_tk("Invalid number", tk);
				]
			]
			if not mid bt str[i] then
				goto invalid_number;
			var was_dot := false;
			while len_greater_than(str, i), mid bt str[i] or str[i] = '.' do [
				if str[i] = '.' then [
					if was_dot then
						goto invalid_number;
					was_dot := true;
					if len_greater_than(str, i + 1), str[i + 1] = '.' then
						break;
					if len_greater_than(str, i + 1), not mid bt str[i + 1] and (str[i + 1] and #df) <> exp then [
						i += 1;
						break;
					]
				]
				i += 1;
			]
			if len_greater_than(str, i), (str[i] and #df) = exp then [
				i += 1;
				if len_greater_than(str, i), str[i] = '+' or str[i] = '-' then
					i += 1;
				var has_exp := false;
				while len_greater_than(str, i), str[i] >= '0' and str[i] <= '9' do [
					i += 1;
					has_exp := true;
				]
				if not has_exp then
					goto invalid_number;
			]
			if len_greater_than(str, i) then [
				c := str[i] and #df;
				if c = 'H' or c = 'S' or c = 'L' or c = 'Q' then
					i += 1;
			]
			if len_greater_than(str, i), (ident_middle_bitmap or num_start_bitmap) bt str[i] then
				goto invalid_number;
			tk.t := token_opt.number.(str[start .. i]);
			result +<= tk;
			next_hints := empty(bytes);
		] else if ident_start_bitmap bt c then [
			var is_macro_expansion := c = '@';
			i += 1;
			while len_greater_than(str, i), ident_middle_bitmap bt str[i] do [
				is_macro_expansion or= str[i] = '@';
				i += 1;
			]
			if len_greater_than(str, i), num_start_bitmap bt str[i] then
				abort compiler_error_tk("Invalid identifier", tk);
			var id := i_encode(str[start .. i]);
			var kw := kwmap[id];
			if not is_uninitialized(kw) then [
				tk.t := kw;
			] else if is_macro_expansion then [
				tk.t := token_opt.macro_expansion.(id);
			] else if is_uninitialized(kw) then [
				tk.t := token_opt.identifier.(id);
			]
			result +<= tk;
			next_hints := empty(bytes);
			if tk.t is k_implementation and len(bracket_stack_pos) = 0 then [
				return result + tokenize_recursive~lazy(str[i .. ], filename, line, column, kwmap, opmap);
			]
		] else if operator_bitmap bt c then [
			var was_slash := 0;
			if c = '/' then
				was_slash += 1;
			i += 1;
			while len_greater_than(str, i), operator_bitmap bt str[i] do [
				if str[i] = '/' then
					was_slash += 1;
				else
					was_slash := 0;
				i += 1;
				if was_slash = 2 then [
					i -= 2;
					break;
				]
			]
			if start < i then [
				var id := i_encode(str[start .. i]);
				var op := opmap[id];
				if is_uninitialized(op) then
					tk.t := token_opt.oper.(id);
				else
					tk.t := op;
				result +<= tk;
				next_hints := empty(bytes);
			]
			if was_slash = 2 then [
				while len_greater_than(str, i), str[i] <> 10 do
					i += 1;
			]
		] else if c = '''' or c = '"' or c = '`' then [
			i += 1;
			var s := empty(byte);
			while true do [
				if not len_greater_than(str, i) then [
unterminated_string:
					abort compiler_error_tk("Unterminated string", tk);
				]
				var x := str[i];
				if x = 10 then
					goto unterminated_string;
				if x = c then [
					i += 1;
					if len_greater_than(str, i), str[i] = x then
						goto next_char;
					break;
next_char:
				]
				s +<= x;
				i += 1;
			]
			if c = '''' then [
				tk.t := token_opt.character.(s);
			] else if c = '"' then [
				tk.t := token_opt.string.(s);
			] else if c = '`' then [
				tk.t := token_opt.unicode.(s);
			]
			result +<= tk;
			next_hints := empty(bytes);
		] else if c = ',' then [
			i += 1;
			tk.t := token_opt.comma;
			result +<= tk;
			next_hints := empty(bytes);
		] else if c = ';' then [
			i += 1;
			tk.t := token_opt.semicolon;
			result +<= tk;
			next_hints := empty(bytes);
		] else if c = '(' or c = '[' then [
			i += 1;
			bracket_stack_pos +<= len(result);
			if c = '(' then [
				bracket_stack_type +<= ')';
				tk.t := token_opt.left_paren;
			] else [
				bracket_stack_type +<= ']';
				tk.t := token_opt.left_bracket;
			]
			result +<= tk;
			next_hints := empty(bytes);
		] else if c = ')' or c = ']' then [
			i += 1;
			var l := len(bracket_stack_type);
			if l = 0 or bracket_stack_type[l - 1] <> c then
				abort compiler_error_tk("Imbalanced bracket", tk);
			var bracket_skip := len(result) - bracket_stack_pos[l - 1];
			result[bracket_stack_pos[l - 1]].bracket_skip := bracket_skip;
			tk.bracket_skip := -bracket_skip;
			if c = ')' then
				tk.t := token_opt.right_paren;
			else
				tk.t := token_opt.right_bracket;
			bracket_stack_type := bracket_stack_type[ .. l - 1];
			bracket_stack_pos := bracket_stack_pos[ .. l - 1];
			result +<= tk;
			next_hints := empty(bytes);
		] else if c = '{' then [
			var bracelevel := 1;
			i += 1;
			while len_greater_than(str, i) and bracelevel > 0 do [
				var d := str[i];
				if d = 10 then [
					line_start := i;
					line += 1;
				] else if d = '{' then [
					bracelevel += 1;
				] else if d = '}' then [
					bracelevel -= 1;
				]
				i += 1;
			]
			if bracelevel > 0 then
				abort compiler_error_tk("Unterminated comment", tk);
		] else if c = '~' then [
			i += 1;
			if len_greater_than(str, i), ident_start_bitmap bt str[i] then [
				i += 1;
			] else [
invalid_hint:
				abort compiler_error_tk("Invalid hint", tk);
			]
			while len_greater_than(str, i), ident_middle_bitmap bt str[i] do
				i += 1;
			if len_greater_than(str, i), num_start_bitmap bt str[i] then
				goto invalid_hint;
			var id := str[start + 1 .. i];
			var l := len(result) - 1;
			if l >= 0 then [
				result[l].post_hints +<= id;
			]
			next_hints +<= id;
		] else [
			abort compiler_error_tk("Invalid character " + ntos(c), tk);
		]
	]

	if len(bracket_stack_pos) <> 0 then
		abort compiler_error_tk("Imbalanced bracket", result[bracket_stack_pos[len(bracket_stack_pos) - 1]]);

	return result;
]

fn tokenize_string(str : bytes, t : tokens) : tokens
[
	var kwmap := get_kwmap;
	var opmap := get_opmap;
	return tokenize_recursive(str, t[0].file, t[0].line, t[0].column, kwmap, opmap);
]

fn tokenize(str : bytes, filename : bytes) : tokens
[
	var kwmap := get_kwmap;
	var opmap := get_opmap;
	return tokenize_recursive(str, filename, 1, 1, kwmap, opmap);
]

fn token_find(tl : tokens, t : token_opt, err : bool) : int
[
	var i := 0;
	while len_greater_than(tl, i) do [
		if ord tl[i].t = ord t then
			return i;
		if tl[i].bracket_skip > 0 then [
			i += tl[i].bracket_skip;
			if not len_greater_than(tl, i) then
			//if not len(tl) > i then
			//if i >= len(tl) then
				abort compiler_error_tk("Imbalanced bracket", tl[i]);
			if ord tl[i].t = ord t then
				return i;
		]
		i += 1;
	]
	if err then [
		abort compiler_error("Expected '" + token_opt_to_string(t, false) + "'", tl);
	]
	return -1;
]

fn token_find_3(tl : tokens, t1 t2 t3 : token_opt, err : bool) : int
[
	var i := 0;
	while len_greater_than(tl, i) do [
		if ord tl[i].t = ord t1 or ord tl[i].t = ord t2 or ord tl[i].t = ord t3 then
			return i;
		if tl[i].bracket_skip > 0 then [
			i += tl[i].bracket_skip;
			if not len_greater_than(tl, i) then
			//if not len(tl) > i then
			//if i >= len(tl) then
				abort compiler_error_tk("Imbalanced bracket", tl[i]);
			if ord tl[i].t = ord t1 or ord tl[i].t = ord t2 or ord tl[i].t = ord t3 then
				return i;
		]
		i += 1;
	]
	if err then [
		var loc := "";
		if len_greater_than(tl, 0) then
			loc := token_location(tl[0]);
		abort compiler_error("Expected '" + token_opt_to_string(t1, false) + "' or '" +  token_opt_to_string(t2, false) + "' or '" +  token_opt_to_string(t3, false) + "'", tl);
	]
	return -1;
]

fn token_find_colon(t : tokens) : int
[
	for i := 0 to len(t) do [
		if t[i].t is colon then [
			if i = 0 then
				abort compiler_error("No declared variable", t);
			return i;
		]
		if not t[i].t is identifier then
			return -1;
	]
	return -1;
]

fn split_tokens(tl : tokens, t : token_opt) : (tokens, tokens)
[
	var b := token_find(tl, t, false);
	if b = -1 then
		return tl, empty(token);
	else
		return tl[ .. b], tl[b + 1 .. ];
]

fn token_assert(tl : tokens, t : token_opt) : unit_type
[
	if not len_greater_than(tl, 0) then
		abort compiler_error("Expected '" + token_opt_to_string(t, false) + "'", empty(token));
	if ord tl[0].t <> ord t then
		abort compiler_error("Expected '" + token_opt_to_string(t, false) + "'", tl);
	return unit_value;
]

fn new_token(t : token, opt : token_opt) : token
[
	t.t := opt;
	t.prev_hints := empty(bytes);
	t.post_hints := empty(bytes);
	t.bracket_skip := 0;
	return t;
]

How to run the Ajla Commander:

Install the packages libgmp-dev and libffi-dev

Download Ajla from https://www.ajla-lang.cz/downloads/ajla-0.2.0.tar.gz

Extract Ajla and compile it with "./configure; make"

Install it with "sudo make install"

Download the Ajla Commander source files and put them to some directory

Run "ajla acmd.ajla"

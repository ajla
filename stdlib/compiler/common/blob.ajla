{*
 * Copyright (C) 2024 Mikulas Patocka
 *
 * This file is part of Ajla.
 *
 * Ajla is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Ajla is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Ajla. If not, see <https://www.gnu.org/licenses/>.
 *}

private unit compiler.common.blob;

uses pcode;

fn blob_length(blob : list(pcode_t)) : int;
fn blob_load(blob : list(pcode_t)) : bytes;
fn blob_store(blob : bytes) : list(pcode_t);

fn blob_to_int(c : list(pcode_t)) : int;
fn int_to_blob(v : int) : list(pcode_t);

implementation

fn blob_length(blob : list(pcode_t)) : int
[
	return 1 + (3 + blob[0] shr 2);
]

fn blob_load(blob : list(pcode_t)) : bytes
[
	var len := blob[0];
	var idx := 1;
	if len < 0 then
		abort internal("negative blob length");
	var val : pcode_t := 0;
	var result := empty(byte);
	for p := 0 to len do [
		if (p and 3) = 0 then [
			val := blob[idx];
			idx += 1;
		]
		result +<= val and 255;
		val shr= 8;
	]

	return result;
]

fn blob_store(blob : bytes) : list(pcode_t)
[
	var pc := list(pcode_t).[ len(blob) ];
	var val : pcode_t := 0;
	for i := 0 to len(blob) do [
		var blob_i : pcode_t := blob[i];
		val or= blob_i shl ((i and 3) * 8);
		if (i and 3) = 3 then [
			pc +<= val;
			val := 0;
		]
	]
	if (len(blob) and 3) <> 0 then
		pc +<= val;
	return pc;
]

fn blob_to_int(c : list(pcode_t)) : int
[
	var res := 0;
	var blob := blob_load(c);
	for i := 0 to len(blob) do [
		var v : int;
		v := blob[i];
		if i >= len(blob) - 1 then [
			if v >= 128 then
				v -= 256;
		]
		res or= v shl i * 8;
	]
	return res;
]

fn int_to_blob(v : int) : list(pcode_t)
[
	var b := empty(byte);
	var last : byte := 0;
	while v <> 0 and v <> -1 do [
		last := v and #ff;
		b +<= last;
		v shr= 8;
	]
	if v = 0 and last bt 7 then
		b +<= 0;
	if v = -1 and not last bt 7 then
		b +<= #ff;
	return blob_store(b);
]

===RELEASE 0.2.0 ===

Fix an internal error if we call dstatfs on a directory open with O_PATH.

Implement automatic inlining of functions with just one basic block.

Implement integer operations with immediate value opperand - so that we don't
have to allocate frame slots for immediates.

Move tests for thunks to machine code entry, so that thunks don't have to be
checked inside loops.

Remember that a pointer is not thunk and do not test it for thunk repetitively.

Implement register allocation in the code generator.

Fix a bug on alpha that the code generator accidentally used saved register f2

=== RELEASE 0.1.5 ===

Fix a bug that swapend.ajla was not present in the distributed archive. It
produced spurious make install error (that could be ignored).

Haiku support - the pthread implementation fails on Haiku (perhaps, there is
some signal race condition in the Haiku pthread implementation), so I added a
new file th_haiku.c.

Code generator improvements - variables' tags are checked when entering machine
code so that they don't have to be checked inside loops.

Fix code generator bugs on power1 and power2.

=== RELEASE 0.1.4 ===

Use the _Float16 type if available

Use -fno-math-errno if the compiler supports it

Use the instructions for floating point rounding when available

Fix a bug in math_round

Define _TIME_BITS=64

Make it possible to compile Ajla with TCC

Swap the endianity of builtin.pcd when the user runs "make install", so that the
file can be mmapped on big-endian machines

Fix a memory leak introduced in the release 0.1.3

Introduce a "--save" argument that disables saving and loading cache

=== RELEASE 0.1.3 ===

Implement API for signal handling.

Support Ctrl-Z for suspending text-mode applications.

Invalidate cached compilations when builtin.pcd changes.

Implement API for listing mount points.

Fix failure on NetBSD when walking the kernfs mount point, because it doesn't
support kqueue.

Introduce a "vselect" widget that displays vertical list of entries.

Incompatible FFI changes:
- Change the FFI interface, so that the destructor is freed explictly with
  ffi_destructor_destroy.
- Delete the function ffi_call_function_pure - ffi_unsafe_get_world and
  ffi_call_function should be used instead.

=== RELEASE 0.1.2 ===

Fix a bug that the tag bit was not checked on the second argument of
len_at_least and len_greater_than - this resulted in incorrect result if the
value was a box or a thunk.

Optimization: don't check index against list length if it can be proved that the
index is smaller (such as "for i := 0 to len(l) do ...").

=== RELEASE 0.1.1 ===

Save the JIT-generated machine code to the files in ~/.cache/ajla. This reduces
startup time.

Introduce stdlib/mutex.ajla

Fix crashes on alpha, loongarch64, mips.

Incompatible change: Fix misdesigned FFI interface - we need to pass the
destructor to the functions that manipulate memory allocated from the
destructor, so that the destructor is not freed prematurely. Previously, the
"keep" statement was used for this purpose, but it is not reliable.

=== RELEASE 0.1.0 ===

Initial release

vim: textwidth=80
